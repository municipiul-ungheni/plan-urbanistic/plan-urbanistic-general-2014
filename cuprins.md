## Volumul I. Soluții de sistematizare. ##

### [1. Întroducere](volumul1/v1_cap1.md) ###
* [1.1. Obiectul lucrării](volumul1/v1_cap1/v1_cap1.1.md)
* [1.2. Colectivul de elaborare](volumul1/v1_cap1/v1_cap1.2.md)
* [1.3. Surse documentare](volumul1/v1_cap1/v1_cap1.3.md)
* [1.4. Scurt istoric](volumul1/v1_cap1/v1_cap1.4.md)
* [1.5 Relații în teritoriu](volumul1/v1_cap1/v1_cap1.5.md)

### [2. Situația hidrogeoconstructivă](volumul1/v1_cap2.md) ###

### [3. Potențialul economic](volumul1/v1_cap3.md) ###
* [3.1. Caracterizarea situației existente](volumul1/v1_cap3/v1_cap3.1.md)
* [3.2. Prognoza potențialului economic](volumul1/v1_cap3/v1_cap3.2.md)

### [4. Populația](volumul1/v1_cap4.md) ###
* [4.1. Situația existentă](volumul1/v1_cap4/v1_cap4.1.md)
* [4.2. Reglementări](volumul1/v1_cap4/v1_cap4.2.md)

### [5. Fondul locativ](volumul1/v1_cap5.md) ###
* [5.1. Situația existentă](volumul1/v1_cap5/v1_cap5.1.md)
* [5.2. Reglementări](volumul1/v1_cap5/v1_cap5.2.md)

### [6. Dezvoltarea infrastructurii sociale](volumul1/v1_cap6.md) ###
* [6.1. Instituții de educație](volumul1/v1_cap6/v1_cap6.1.md)
* [6.2. Obiective de ocrotirea sănătății](volumul1/v1_cap6/v1_cap6.2.md)
* [6.3. Obiective cultură, sport și agrement](volumul1/v1_cap6/v1_cap6.3.md)
* [6.4. Obiective comerț, alimentație publică și deservire comunală](volumul1/v1_cap6/v1_cap6.4.md)

### [7. Turism](volumul1/v1_cap7.md) ###
* [7.1. Situația existentă.](volumul1/v1_cap7/v1_cap7.1.md)
* [7.2. Dezvoltarea turismului municipiului Ungheni](volumul1/v1_cap7/v1_cap7.2.md)

### [8. Zonarea teritoriului](volumul1/v1_cap8.md) ###
* [8.1. Situația existentă](volumul1/v1_cap8/v1_cap8.1.md)
* [8.2. Reglementări](volumul1/v1_cap8/v1_cap8.2.md)

### [9. Organizarea zonei industriale](volumul1/v1_cap9.md) ###
* [9.1. Situația existentă](volumul1/v1_cap9/v1_cap9.1.md)
* [9.2. Reglementări](volumul1/v1_cap9/v1_cap9.2.md)
* [9.3. Concluzii](volumul1/v1_cap9/v1_cap9.3.md)

### [10. Căi de comunicații și transport](volumul1/v1_cap10.md) ###
* [10.1. Situația existentă](volumul1/v1_cap10/v1_cap10.1.md)
* [10.2. Reglementări](volumul1/v1_cap10/v1_cap10.2.md)

### [11. Unele permisiuni și restricții. Reglementări](volumul1/v1_cap11.md) ###

### [12. Concluzii. Măsuri de continuare](volumul1/v1_cap12.md) ###

### [13. Indicatorii de bază](volumul1/v1_cap13.md) ###
* [13.1. Situația existentă](volumul1/v1_cap13/v1_cap13.1.md)
* [13.2. Reglementări](volumul1/v1_cap13/v1_cap13.1.md)

## Volumul II. Asigurarea tehnico-edilitatea. Protecția mediului. Protecția civilă. ##

### [14. Alimentarea cu apă potabilă](volumul2/v1_cap14.md) ###
* [14.1. Situația existentă](volumul2/v2_cap14/v2_cap14.1.md)
* [14.2 Reglementări](volumul2/v2_cap14/v2_cap14.2.md)

### [15. Canalizare menajere](volumul2/v1_cap15.md) ###
* [15.1. Situația existentă](volumul2/v2_cap15/v2_cap15.1.md)
* [15.2 Reglementări](volumul2/v2_cap15/v2_cap15.2.md)

### [16. Canalizare pluviale](volumul2/v1_cap16.md) ###
* [16.1. Situația existentă](volumul2/v2_cap16/v2_cap16.1.md)
* [16.2 Reglementări](volumul2/v2_cap16/v2_cap16.2.md)

### [17. Salublizarea teritoriului](volumul2/v1_cap17.md) ###
* [17.1. Situația existentă](volumul2/v2_cap17/v2_cap17.1.md)
* [17.2 Reglementări](volumul2/v2_cap17/v2_cap17.2.md)

### [18. Alimentare cu energie electrică](volumul2/v1_cap18.md) ###
* [18.1 Situația existentă](volumul2/v2_cap18/v2_cap18.1.md)
* [18.2 Reglementări](volumul2/v2_cap18/v2_cap18.2.md)

### [19. Alimentare cu energie termică](volumul2/v1_cap19.md) ###
* [19.1 Situația existentă](volumul2/v2_cap19/v2_cap19.1.md)
* [19.2 Reglementări](volumul2/v2_cap19/v2_cap19.2.md)

### [20. Alimentare cu gaze naturale](volumul2/v1_cap20.md) ###
* [20.1 Situația existentă](volumul2/v2_cap20/v2_cap20.1.md)
* [20.2 Reglementări](volumul2/v2_cap20/v2_cap20.2.md)

### [21. Protecția mediului](volumul2/v1_cap21.md) ###
* [21.1 Situația existentă](volumul2/v2_cap21/v2_cap21.1.md)
* [21.2 Reglementări](volumul2/v2_cap21/v2_cap21.2.md)

### [22. Protecția civilă](volumul2/v1_cap22.md) ###