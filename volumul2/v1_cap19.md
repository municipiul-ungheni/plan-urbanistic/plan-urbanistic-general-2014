< Înapoi la [Cuprins](../cuprins.md)

# 19. Alimentare cu energie termică #
* [19.1 Situația existentă](v2_cap19/v2_cap19.1.md)
* [19.2 Reglementări](v2_cap19/v2_cap19.2.md)

< Înapoi la [Cuprins](../cuprins.md)