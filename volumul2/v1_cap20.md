< Înapoi la [Cuprins](../cuprins.md)

# 20. Alimentare cu gaze naturale #
* [20.1 Situația existentă](v2_cap20/v2_cap20.1.md)
* [20.2 Reglementări](v2_cap20/v2_cap20.2.md)

< Înapoi la [Cuprins](../cuprins.md)