< Înapoi la [Cuprins](../cuprins.md)

# 18. Alimentare cu energie electrică #
* [18.1 Situația existentă](v2_cap18/v2_cap18.1.md)
* [18.2 Reglementări](v2_cap18/v2_cap18.2.md)

< Înapoi la [Cuprins](../cuprins.md)