< Înapoi la [Cuprins](../../cuprins.md) | [8. Zonarea teritoriului](../v1_cap8.md)

# 8.1 Situaţia existentă. #
Oraşul Ungheni este localizat în partea de vest şi centru a Republicii Moldova, pe
malul stîng al rîului Prut şi în imediata apropiere a graniţei cu Romînia. Orașul se
află la o distanţă de 105 km de municipiul Chișinău, la 85 km de or. Bălţi şi la 45 km
de or. Iaşi (pe automagistrală şi la 21 km pe calea ferată). Ungheni-ul are o reţea de
drumuri de importanţă naţională şi locală bine dezvoltată, fiind un important nod
feroviar şi punct de frontieră vamal ce conectează reţeaua internă cu exteriorul. Staţia
de cale ferată Ungheni este punct internaţional de trecere; se efectuează transport de
mărfuri şi de pasageri. Acesta reprezintă cel mai mare nod de cale ferată din centrul
ţării.
Orașul dispune şi de un port fluvial pe r-ul Prut care se află în gestiunea
Întreprinderii de Stat ”Portul Fluvial Ungheni”. Portul fluvial Ungheni este unul din
cele patru porturi fluviale existente în prezent în Republica Moldova.
Orşul Ungheni este extins pe o lungime de 9 km pe malul stîng al rîuluI Prut, si are
o suprafaţă totală de 1643 ha. Din suprafaţa dată fac parte şi două rîuleţe numite
Delia şi Băileşti. Rîul Delia are scurgerile din şireagul de dealuri ale Basarabiei
centrale, iar rîul Băileşti trece prin sudul oraşului şi se varsă în rîul Prut.
În afară de rîuri în localitate mai sunt trei lacuri cu o suprafaţă totală de 125ha: Delia,
Ceachir şi Bereşti. Lacul Delia se află la 1km de la gura de vărsare a rîului, are o
lungime de 3,2km şi o adîncime de 4,0m.
Oraşul este divizat în 7 sectoare:
I. Sectorul ,,Central,, care constituie nucleul orașului este situat în centrul acestuia
şi ocupă o suprafaţă de 350ha.
II. Sectorul ,,Bereşti,, este amplasat pe malul Prutului. Suprafaţa acestui sector este
de 240ha.
III. Sectorul ,,Ungheni Vale,,. Apelativul şi l-a luat de la aşezarea sa geografică, în
valea Prutului, faţă de celelalte zone ale municipiului, şi dispune de o suprafaţă de
133ha.
IV. Sectorul ,,Dănuţeni,, sectorul se află pe malul Prutului, la gura de vărsare a
rîului Băileşti cu suprafaţa de 470ha.45
V. Sectoarul ,,Ungheni Deal,, botezat astfel datorită poziţiei sale geografice, mai
ridicat, pe o colină, în raport cu întreg orașul, şi ocupă o suprafaţă totală de 200ha.
VI. Sectorul ,,Industrial,,. Denumirea vine însuşi de la zona situată în acest sector,
supafaţa lui este de 190ha.
VII. Sectorul ,,Tineretului,, cel mai nou sector, ocupă cea mai mică suprafaţă 60ha.
Fiecare sector în parte dispune de dotări ce includ deservirea populaţiei. Din ele fac
parte:
- dotări administrative;
- dotări culturale, sport, turism;
- dotări de învăţămînt;
- dotări sanitare şi asistenţă socială;
- dotări comerciale, prestări servicii;
- dotări comunale;
- dotări cu destinaţie specială;
- dotări religie, cult, monumente.
Oraşul mai dispune de zonă industrială diversificată şi foarte bine dezvoltată
fiind reprezentată de întreprinderi din industria alimentară, uşoară, de confecţii şi a
materialelor de construcţie. Resursele de muncă în industrie, estimate la 1 ianuarie
2003, constituie 2.167 persoane. În sectorul industrial activează peste 38 de
întreprinderi.
Repartizarea dotărilor în funcţie de amplasament în cadrul sectoarelor:
I Sectorul ,,Central,, - Suprafaţa acestui teren ocupat de sectorul menţionat
este de 350ha.
a). Dotări admininstrative.
- Primăria
- Procuratura
- Judecătoria
- Consiliul raional
- Serviciul de informaţie şi securitate al Republicii Moldova
- Ministerul apărării
- Direcţia raională agricultură şi alimentare
- Direcţia pentru statistică
- Agenţia teritorială
- Apă canal
- Secţia de poliţie
- Poliţia rutieră
- Oficiul poştal
- Banca de economii
- Agenţia ,,Moldsilvica,,.46
b). Dotări culturare, sport, turism
- Casa de cultură (2 obiective);
- Cinematograf (2 obiective, actualmente unul din ele nu funcţionează);
- Agenţie de turism;
- Biblioteca;
- Hotel.
Edificiul central sportiv al or. Ungheni reprezintă stadionul orăşănesc situat în
cenntrul oraşului, asigurat cu un cîmp de fotbal şi cu tribune pentru spectatori.
c). Dotări de învăţămînt
● Instituţii superioare de învăţămînt:
- Colegiul de medicină;
● Instituţii de studii generale:
- Liceul teoretic V. Alecsandri;
- Liceul teoretic M. Eminescu;
- Şcoala primară S. Vangheli;
- Şcoala de arte;
- Şcoala muzicală;
- Centrul educaţional.
● Instituţii preşcolare:
Sectorul ,,Central,, dispune de 2 grădiniţe de copii ambele sunt în funcţiune.
d). Dotări sanitare şi asistenţa socială
- Policlinica;
- Asistenţa medicală de urgenţă;
- Spitalul raional;
- Ambulator;
- Centrul medical ,,Alfamed,,
- Farmacii;
- Stomatolagii;
- Farmacia veterinară.
e). Dotări comerciale prestări servicii
În această sferă după mărime şi importanţă în aceast sector se evidenţiază:
Centrele comerciale (supermarket) care sunt mai multe la număr, un magazin
universal, 2 restaurante.
Piaţa centrală Coopco situată pe str. Alexandru cel Bun, tot aici este amplasată şi
piaţa de animale. Din dotările date care se află în sectorul ,,Central,, mai fac parte şi
următoarele obiective de deservire a populaţiei, mai multe magazine alimentare,
magazine de mărfuri industriale, magazine agricole, cafenele, baruri, pizzării.
f). Dotări comunale
Suprafaţa acestei zone este de 14,2ha. Aici sunt incluse:
- Staţia de alimentare cu combustibil;
- Staţia epidemiologică;
- Staţia de alimentare cu apă;
- Cazangeriile – deservesc unele grupe de blocuri locative multietajate, cît şi
obiectivele de utilitate publică, şi sunt situate în preajma acestora;
- Cimitir- 3,25;
- La fel şi garajele sunt situate în apropierea blocurilor locative multietajate pe
pante libere de construcţie.47
g). Dotări cu destinaţie specială
Toate obiectivele cu această destinaţie sunt situate în sectorul ,,Central,,.
- Autogara;
- Gara feroviară internaţională;
- Gara feroviară interurbană;
- Armata naţională;
- Centrul teritorial militar;
- Pompieri.
i). Dotări religie, de cult, monumente
Oraşul dispune de mai multe obiective de cult. O parte din ele sunt situate în
sectorul ,,Central,,.
- Biserica ,,Sfîntul Alexandru Nevschi,, (monument arhitectural şi istoric) anul
1905.
- Biserica ,,Acoperămîntul Maicii Domnului,,
- Biserica ,,Lumina Luminii,,
- Mănăstirea ,,Sfîntul Gheorghe,,
- Parorhia Sfîntă Catolică.
Majoritatea monumentelor sunt amplasate în zona centrală a urbei, ceia ce în mare
măsură a determinat formarea structurii arhitecturale – planimetrice şi compoziţiei
spaţiale a fondului construit. Obiectele menţionate oferă nucleului urban şi funcţiile
de centru istoric.
- Monument ,,La mormîntul ostaşilor căziţi,, (566) în anul 1944 – monument
istoric.
- Monument ,,În memoria pămîntenilor căzuţi în 1941-45.
- Monument ,,Ostaşilor căzuţi,, în anul 1947.
- Monument ,,Bustul lui Mihai Eminescu,,.
II Sectorul ,,Bereşti,, - suprafaţa acestui sector este de 240ha.
a). Dotări admininstrative
- Poliţia rutieră;
- Inspectoratul ecologic de stat.
b). Dotări culturare, sport, turism

Cinematograf.
c). Dotări de învăţămîn
Sectorul menţionat nu dispune de dotări de învăţămînt.
d). Dotări sanitare şi asistenţa socială.
- Centrul de reabilitare;
- Centrul de integritate a tineretului.
e). Dotări comerciale prestări servicii.
- Magazin agricol;
- Magazin alimentar;
- Magazin piese auto; - Casa de ceremonii.48
f). Dotări comunale.
- Garaje;
- Sondă arteziană – 4 la număr;
- Cimitir cu suprafaţa de 0,74ha.
g). Dotări cu destinaţie specială.
Astfel de dotări nu avem în subzona dată.
i). Dotări religie, de cult, monumente.
- Biserica
III Sectorul ,,Ungheni Vale,,- suprafaţa de 133ha,
c). Dotări de învăţămînt. - Şcoala auto;
- Grădiniţa de copii;
e). Dotări comerciale prestări servicii.
- Magazin alimentar;
- Spălătorie şi curăţătorie.
f). Dotări comunale.
- Staţie de pompare a apelor.
g). Dotări cu destinaţie specială.
- Direcţia portului fluvial.
IV Sectorul ,,Dănuţeni,, - suprafaţa totală de 470ha este unul
din cele mai mari sectoare.
a). Dotări admininstrative
- Inspectoratul de poliţie;
- Oficiul poştal.
b). Dotări culturare, sport, turism
- Biblioteca;
- Secţia evidenţă şi documentare a populaţiei.
c). Dotări de învăţămînt.
- Liceul teoretic Ion Creangă;
- Şcoala profesională;
- Grădiniţa de copii.
d). Dotări sanitare şi asistenţa socială
- Spitalul orăşănesc.
e). Dotări comerciale prestări servicii
- Magazin alimentar.
f). Dotări comunale.
- Centrala termică;
- Staţia de pompare.
- Cimitir cu suprafaţa 1,76ha.
i). Dotări religie, de cult, monumente.
- Biserica Sfîntul Nicolae.49
V Sectorul ,,Ungheni Deal,, - Suprafaţa totală este de 200ha.
Sectorul dispune de terenuri ocupate de locuinţe în
regim mic de înălţime. Cea mai mare parte a teritoriului este ocupată de lacul Delia
care are o suprafaţă de 110,9ha.
VI Sectorul ,,Industrial,, - Are suprafaţa de 110ha.
Din care:
b). Dotări culturare, sport, turism
- Hotel;
e). Dotări comerciale prestări servicii
- Restaurant;
- Cafenea.
f). Dotări comunale
Suprafaţa ocupată de această zonă este de 2,7ha. Din dotările existente fac parte:
- Staţia de transformare a energiei electrice;
- Garaj;
- Centrala termică.
VII Sectorul ,,Tineretului,, - Întreaga suprafaţă a acestui sector este de 60ha.
c). Dotări de învăţămînt
- Colegiul agroindustrial;
- Liceul teoretic A.S.Pişchin;
- Liceul teoretic G.Asachi;
- Şcoala primară S.Vangheli;
- Şcoala de arte.
- în acest sector sunt situate 3 grădiniţe de copii
d). Dotări sanitare şi asistenţa socială
- Stomatologie;
- Farmacie.
e). Dotări comerciale prestări servicii
- Casa de ceremonii;
- Piaţa Coopco;
- Casa de ceremonii;
- Cafenea, bar, pizzarie;
- Restaurant;
- Magazin alimentar.
f). Dotări comunale
- Cazangerie.
i). Dotări religie, de cult, monumente
- Biserica ,,Întîmpinarea Domnului,,;
- Casa de rugăciuni;
- Monument G.Asachi.

Aspecte caracteristice ale principalelor zone funcţionale:
Zone de locuit, figurată pe planşa nr. 2 în culoarea galben este diferenţiată pe două
tipuri de locuinţe: înalte, mai mari de P+4 marcate cu galben închis şi joase P şi P+1
marcate cu galben deschis. Vorbind despre această zonă putem menţiona că sunt
dispuse într-un număr de 6 cartiere astfel:
CARTIERUL CENTRAL
10.970 locuitori
densitatea 142 loc/ha
3493 apartamente
16,85 mp Au/loc.
CARTIERUL TINERETULUI
12.800 locuitori
densitatea 267 loc/ha
4448 apartamente
16,74 mp Au/loc
CARTIERUL BERESTI
3.37 locuitori
densitatea 23 loc/ha
1060 apartamente
17,46 mp Au/loc
CARTIERUL UNGHENI-VALE
1600 locuitori
densitatea 20 loc/ha
515 apartamente
17,33 mp Au/loc
CARTIERUL UNGHENI-DEAL
1070 locuitori
densitatea 26 loc/ha
339 apartamente
17,50 mp Au/loc
CARTIERUL DĂNUŢENI
8.380 locuitori
densitatea 31 loc/ha
2864 apartamente
17,29 mp Au/loc
Cartierul Bereşti situat în partea de nord-est a oraşului şi Dănuţeni situat în
partea de sud a oraşului sunt provenite din rate cuprinse în intravilan ca urmare a
dezvoltării economice. Ca urmare a acestui fapt precizăm că ele se compun din
locuinţe în stare mai rea de întreţinere sau în stare mediocră (în majoritatea cazurilor)
de asemenea construcţiile din aceste cartiere au o vechime de peste 55 de ani ceea ce
dovedeşte de asemeni existenţa şi formarea oraşului cu preponderenţă în zona
centrală, pe malul drept al fostului râu Delia, actualmente lacul Delia.
Cartierul Central situat pe malul drept al lacului Delia, include şi cele mai
multe dotări cu caracter social, cultural şi administrativ, care deservesc
nu numai oraşul, dar unele chiar şi raionul. În vecinătatea nordică a acestuia este
situat cartierul Tineretului, cartier cu locuinţe colective, cu cea mai mare densitate
din oraş incluzând de asemeni obiective publice de interes local (şcoli, grădiniţe etc).58
Cartierul Ungheni Deal şi Vale au un caracter semiurban, unul de
provenienţă mai veche (Vale) şi altul de provenienţă mai recentă (Deal). Ambele au
construcţii de locuinţe cu loturi individuale.
Zona activităţilor economice este situată în partea de nord a oraşului fiind zona
economică liberă.
Tot în această parte a oraşului, dar situată în afara intravilanului se află „Fabrica de
covoare” considerată cea mai importantă din Republica Moldova. Deasemeni tot în
extravilan este situat şi S.A. Ceriale Prut.
În afara aceste zone majore de activităţi economice mai există unităţi dispersate de
construcţii, transporturi sau depozitare: de construcţii în cartierul Bereşti, de
prelucrare a lemnului în Ungheni Vale, de construcţii în Dănuţeni. Tot în Dănuţeni,
în sudul cartierului se află o zonă de industrie prezentată de fosta uzina biochimică,
în fază de reorganizare.
Zona feroviară include liniile ferate cu destinaţia Iaşi-Bălţi şi Chişinău în
construcţiile aferente acesteia sunt: gara internaţională şi gara locală. Deasemeni în
extravilan în partea de nord-est este situată gara Bereşti care deserveşte ruta
Chişinău-Bălţi. Tot în extravilan în partea de est este situată gara Unţeşti având cu
preponderenţă rol de depozitare marfa.
Zona spaţiilor verzi din oraşul Ungheni este reprezentată pe planşă cu verde
deschis şi include: parcuri, terenuri sportive şi de agrement.
Principala zonă de agrement a oraşului Ungheni este reprezentată de malul
stâng al lacului Delia zonă însă neamenajată incluzând o porţiune de plajă şi altă
porţiune de pădure tânără plantată în scopul opririi alunecării de teren.
De asemeni neamenajată este şi porţiunea din cartierul Ungheni Vale, cuprinsă
între linia de cale ferată şi dâmbul pe care se situează Casa de cultură.
Crtierele Dănuţeni şi Bereşti nu dispun de nici un fel de spaţii verzi, la fel şi
Cartierul Tineretului care are numai un mic teren sportiv.
Cartierul Central dispune de un mic parc amenajat pe malul dreapta al lacului Delia.
Obiectivele de interes public, în oraşul Ungheni, includ atât dotări social-
culturale cât şi locuri edilitare (apă, canalizare, termice, telefonie) şi gospodăreşti
(cimitire, groapă de gunoi).
Dintre dotările de interes public enumerăm: o casă de cultură, un cinematograf,
o judecătorie, 2 biblioteci, 2 hoteluri, o autogară, 2 gări feroviare, 2 magazine
generale, un spital raional şi un spital orăşenesc (maternitate).

< Înapoi la [Cuprins](../../cuprins.md) | [8. Zonarea teritoriului](../v1_cap8.md)