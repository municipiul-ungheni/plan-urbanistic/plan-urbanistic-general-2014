< Înapoi la [Cuprins](../../cuprins.md) | [8. Zonarea teritoriului](../v1_cap8.md)
# 8.2 Reglementări #
a) Propuneri de proiect
Proiectul Planului urbanistic general al orașului Ungheni prevede dezvoltarea în
continuare a structurii de sistematizare, deja formate cu îmbinarea sectoarelor de
sistematizare într-un spaţiu unic, care convenţional include 7 formaţiuni urbane
existente - de producere, teritorii de recreere şi agrement, precum şi obiective ale
infrastructurii edilitare, asigurate cu legături de transport fiabile.
Ideea principală a Planului urbanistic general constă în crearea unei structurii
orăşeneşti urbanizate, compacte, îmbinate, echilibrate, care va asigura dezvoltarea
durabilă a elementelor mediului şi oraşului în general, atât pentru perioada de calcul,
cât şi pentru perspectivă.59
Proiectul prevede perfecţionarea continue a principiilor zonificării funcţionale,
în deosebi formarea sistemului centrelor publice, atât în formaţiunile locuibile
existente, cât şi în cele noi. Aceasta va permite într-o oarecare măsură eliberarea
nucleul centrului oraşului, amplasarea obiectivelor prestări servicii în apropierea
zonelor rezidenţiale şi de producere.
Reconstrucţia centrului prevede - construcţia grupurilor separate de clădiri de
mare înălţime cu obiective prestări servicii cu accente urbanistice care vor finisa
organizarea compoziţiei arhitectural-spaţiale a oraşului. Se prevede crearea brâului
verde unic a oraşului cu obiective de agrement, sport şi recreere, care constituie
elementul de bază în formarea spaţiului arhitectural - urbanistic unic a oraşului.
Dezvoltarea reţelei de drumuri şi străzi propusă se axează pe schema deja
formată cu dezvoltarea şi perfecţionarea ei ulterioară. Formarea reţelei de străzi de
dublare şi suplimentare pentru stabilirea legăturilor de transport între formaţiunile
locative, construcţia drumurilor spre sectoarele cu construcţii locative noi, construcţia
centurii de ocolire va da posibilitatea reorganizarii şi optimizarea tuturor tipurilor de
transport urban şi public, la maxim va elibera centru oraşului din contul reducerii şi
descentralizării intensităţii circulaţiei, şi în primul rând a transportului în tranzit. Se
prevede sporirea lungimii reţelei de drumuri şi străzi cu 39 km, inclusiv a străzilor
magistrale cu 27 km.
Conform proiectului se prevede reorganizarea teritoriilor întreprinderilor de
producere în hotarele existente a zonelor industriale, lista este reflectată pe planșa
anexată - zonificarea funcţională a teritoriului. Reconstrucţia, modernizarea
tehnologică şi reutilarea tehnică a întreprinderilor va permite fabricarea producţiei de
înaltă calitate şi competitive pe piaţa internă şi externă. Implementarea tehnologiilor
noi de producere va diminua impactul negativ asupra mediului, astfel, asigurând
respectarea normativelor sanitare şi ecologice.
Proiectul Planului urbanistic general preconizează extinderea teritoriului destinat
pentru construcţia caselor individuale cu 50 ha din contul construcţiei caselor locative
multietajate (5-9 nivele) şi caselor de tip cotedj.
Soluţiile de sistematizare arhitecturală propuse privind amplasarea construcţiilor de-a
lungul zonei riverane a r. Prut în îmbinare cu obiectivele patrimoniului cultural şi
istoric, instituţiile prestări servicii, elementele arhitecturii landşaftologice vor
contribui la formarea aspectului arhitectural şi estetic în zona de centru a oraşului.
Realizarea proiectului de amenajare a teritoriului nominalizat va asigura crearea
condiţiilor optime pentru recreere şi agrement atât pentru locuitorii oraşului, cât şi
pentru turiştii.
b) Zonarea spaţului în perspectiva
Una din sarcinile de bază în procesul de reamenajare a localităţii reprezintă
salubrizarea şi înverzirea, care contribuie la sporirea nivelului arhitectural şi
ameliorarea stării sanitaro-igienice a teritoriului. Salubrizarea şi înverzirea teritoriului
într-o oarecare măsură contribuie la îmbunătăţirea condiţiilor de abitaţie, culturale şi
de producere a populaţiei.
Salubrizarea teritoriului cu construcţii prevede organizarea unui complex de
măsuri, care include protecţia mediului şi plantaţii verzi, îmbunătăţirea condiţiilor
naturale prin crearea microreliefului, plantarea arborilor şi arbuştilor decorativi,
amenajarea gazoanelor, florăriilor, bazinelor artificiale de apă, havuzurilor,
asigurarea cu mobilier urban.68
Influenţa optimă a zonei verzi asupra condiţiilor sanitaro-igienice de abitaţie şi
asupra aspectului arhitectural-artistic al oraşului se poate obţine din contul amplasării
uniforme şi continue a zonelor verzi pe teritoriul oraşului.
Acest sistem reprezintă prin sine o zonă verde spaţial încadrată cu diversă
destinaţie funcţională. Acestea sunt sectoarele cu plantaţii verzi ale grupurilor
locative, pe teritoriul instituţiilor publice, parcuri, străzi, scuaruri, grădini, precum şi
verdeaţa landşaftului înconjurător. Conform proiectului se prevede extinderea unor
zone separate de parc.
Concomitent se propune crearea spaţiilor verzi din contul reamenajării iazului
Delia. Se propune de unit aceasta zonă cu teritoriul complexului sportiv orăşenesc ce
se va afla in lunca inundabila a r. Delia, construcţii sportive de tip deschis şi blocul
sportiv acoperit.
Unirea zonelor verzi cu destinaţie funcţională diversă într-un sistem unic se va
efectua din contul înverzirii străzilor drumurilor şi bulevardelor. La crearea zonelor
verzi noi şi reamenajarea celor existente se propune diversificarea asortimentului
arborilor şi arbuştilor cu introducerea speciilor rezistente la secetă şi longevivi.
Elementele de amenajare a străzilor şi drumurilor (indicatoare rutiere, parcări
auto, peron de staţie acoperit, teren de agrement, sculpturi decorative,
memorabile) trebuie să fie executate în stil tradiţional şi constructiv. Parte
componentă a salubrizării străzilor reprezintă gazoanele, florăriile, plantarea
arborilor în rând sau grupuri, bulevarduri.
< Înapoi la [Cuprins](../../cuprins.md) | [8. Zonarea teritoriului](../v1_cap8.md)