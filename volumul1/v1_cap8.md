< Înapoi la [Cuprins](../cuprins.md)

# 8. Zonarea teritoriului #
* [8.1. Situația existentă](v1_cap8/v1_cap8.1.md)
* [8.2. Reglementări](v1_cap8/v1_cap8.2.md)

< Înapoi la [Cuprins](../cuprins.md)