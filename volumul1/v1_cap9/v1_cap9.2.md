< Înapoi la [Cuprins](../../cuprins.md) | [9. Organizarea zonei industriale](../v1_cap9.md)

# 9.2 Reglementări #
Pornindu-se de la analiza situaţiei existente a complexului industrial şi luând în
consideraţie soluţiile de actualizare a Planului Urbanistic General în condiţiile
economiei de piaţă au fost elaborate propuneri de amenajare şi amplasare a teritoriilor
zonelor industriale.
Asupra propunerilor s-a consultat cu administraţia publică locală a or. Ungheni,
în urma cărora au apărut unele obiecţii şi completări la prezentul proiect.
În context, propunerile de sistematizare a or. Ungheni la compartimentul
“Industrie” se păstrează în esenţa structura anterior creată a zonelor industriale, care
sunt correlate la soluţiile urbanistice ale Planului General or. Ungheni.
Lista întreprinderilor industriale propuse spre reamplasare sau reorganizare şi
teritoriile de rezervă sunt indicate în tabelele nr.9.2.3; 9.2.4.
După cum s-a menţionat în compartimentul “Date generale”, “Starea actuală”
oraşul Ungheni dispune de 5 zone sectoarele:
Zona industrială, sectorul „Centrul”;
Zona industrială, sectorul „Bereşti”;
Zona industrială, sectorul „Ungheni Vale”;
Zona industrială, sectorul „Danuţeni”;
Întreprinderile aceastor zone sunt amplasate adiacent cu cartierele locative şi vor
fi păstrate parţial activităţile lor conform soluţiilor Planului Urbanistic General.
Excepţie prezintă întreprinderile, care nu se conformă normelor sanitaro-ecologice,
care sunt reflectate în tab. nr. 9.2.4.
2 Sectorul „Zona Industrială”
Majoritatea întreprinderilor, amplasate în această zonă, sunt în „Zona
Economică Liberă” parcelată în trei subzone care activează în diferite domenii de
producere: alimentară, echipamente pentru autovehicule, materiale de construcţie,
produse cosmetice şi chimice, producerea mobilei şi prelucrarea lemnului, covoare
ş.a.71
Întreprinderile în afara ZEL cu activităţi reduse, se propun spre reorganizarea
sau reprofilare ţinînduse cont cerinţele ecologice şi de soluţiile “Planului Urbanistic General”.
Aici sunt formate şi teritorii de dezvoltare în perspectivă.



< Înapoi la [Cuprins](../../cuprins.md) | [9. Organizarea zonei industriale](../v1_cap9.md)