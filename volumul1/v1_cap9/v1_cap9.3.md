< Înapoi la [Cuprins](../../cuprins.md) | [9. Organizarea zonei industriale](../v1_cap9.md)

9.3 Concluzii
În conformitate cu studiile de cercetare şi examinare, efectuate pe teren,
majoritate întreprinderilor a oraşului Ungheni au fost amplasate şi construite în a.a.
1970-1985 şi nu au suportat schimbări esenţiale în structura urbanistică. În proiect s-
au determinat graniţele zonelor industriale, teritoriile de rezervă privind dezvoltarea
de perspectivă, soluţii de reamplasare din sectorul locativ şi informaţii privind
necesităţile inginereşti a întreprinderilor existente.
Datele privind lista întreprinderilor existente, indicii tehnico-economici, lista
întreprinderilor propuse spre reamplasare şi terenurilor de dezvoltare în perspectiva
sunt prezentate în tabelele nr.9.1.l, nr.9.2.2, nr.9.2.3, nr.9.2.4.

< Înapoi la [Cuprins](../../cuprins.md) | [9. Organizarea zonei industriale](../v1_cap9.md)