< Înapoi la [Cuprins](../cuprins.md)

# 9. Organizarea zonei industriale #
* [9.1. Situația existentă](v1_cap9/v1_cap9.1.md)
* [9.2. Reglementări](v1_cap9/v1_cap9.2.md)
* [9.3. Concluzii](v1_cap9/v1_cap9.3.md)

< Înapoi la [Cuprins](../cuprins.md)