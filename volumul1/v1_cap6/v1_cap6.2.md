< Înapoi la [Cuprins](../../cuprins.md) | [6. Dezvoltarea infrastructurii sociale](../v1_cap6.md)

# 6.2. Obiective de ocrotirea sănătăţii #
Compartimentul dat este considerat unul din elementele principale din
activitatea locuitorilor şi securităţii umană a ţării. De starea sănătăţii depinde în mod
direct nivelul bunăstării individuale şi gradul de dezvoltare umană a oraşului.
Obiectele de ocrotire a sănătăţii ocupă o poziţie prioritară în infrastructura
socială. Conform datelor prezentate de instituţiile ocrotirii sănătăţii locale în oraş
funcţionează IMSP Spitalul Raional Ungheni cu – 345paturi, la 1000 locuitori revin 9
paturi. Numărul total al angajaţilor constituie 519 persoane, inclusiv 42 medici. În
cadrul spitalului activează Centru Diagnostic Consultativ cu capacitatea 850
vizite/schimb, de facto numărul vizitelor constituie 958. Numărul scriptic al
angajaţilor – 139, inclusiv 51 medici.

Concomitent cu acestea în oraş funcţionează instituţii medicale de tip ambulator
unde activează 234 angajaţi, inclusiv 35 medici.
Este necesar de menţionat faptul că în oraş funcţionează un şir de farmacii,
inclusiv şi farmacii veterinare.
Capacitatea obiectelor ocrotirii sănătăţii nu se conformează normativelor în
vigoare, în oraş se resimte insuficienţa de calitate, diversitate şi oportunitate în
serviciile prestate, care la rîndul său se reflectă negativ asupra sănătăţii populaţiei.
Pentru perspectivă, conform calculelor, se propune sporirea capacităţii
instituţiilor prestatoare de servicii de sănătate cu 270 vizite /schimb.
În cadrul acestor instituţii se prevede crearea instituţiei medico-sociale de
tratare, recuperare şi reabilitare medicală şi socială a persoanelor cu dizabilităţi.
Crearea Centrului de reabilitare "Şanse egale de viaţă pentru fiecare". Concomitent se
preconizează reparaţia Centrului de reabilitare de zi la Centrul de Sănătate Ungheni.
Către anul 2030 – construcţia centrului de reabilitare şi integrare socială a bătrînilor
cu capacitatea – 600 locuri.

< Înapoi la [Cuprins](../../cuprins.md) | [6. Dezvoltarea infrastructurii sociale](../v1_cap6.md)