< Înapoi la [Cuprins](../../cuprins.md) | [6. Dezvoltarea infrastructurii sociale](../v1_cap6.md)

# 6.4. Obiective comerţ, alimentaţie publică şi deservire comunală #

Reţeaua obiectelor de comerţ şi alimentaţie publică după diversitatea mărfurilor
şi amplasare optimă pe teritoriul oraşului trebuie să se apropie de solicitările
locuitorilor pentru subiectul dat. În prezent în or. Ungheni suprafaţa obiectivelor
comerciale constituie 28,5 mii m 2 suprafaţă totală, inclusiv 15,0 mii m 2 suprafaţă
comercială, fiind reprezentată prin centre comerciale şi construcţii provizorii
(chioşcuri, gherete) sau construcţii adaptate care nu dispun de dotare edilitară
necesară. La 1000 locuitori revin 390 m 2 suprafaţa comercială.
Capacitatea totală a obiectivelor de alimentare publică (cafenea-baruri)
constituie 2200 locuri, ceea ce constituie 58 locuri la 1000 locuitori.
Actualmente, majoritatea obiectivelor comerciale şi alimentare publică sunt
concentrate în partea centrală a oraşului, iar numărul obiectivelor şi calitatea
serviciilor prestate nu corespund exigentelor în vigoare.
Calculul obiectelor comerciale pentru perspectivă este determinat, reieşind din
normativele minim – necesare pentru aceste obiective – 300m 2 la 1000 locuitori, iar
pentru obiectivele alimentare publică – 40 locuri.
Ţinîndu-se cont de calculul numărului populaţiei şi normativele în vigoare atît
pentru perioada existentă, cît şi pentru perspectivă suprafaţa obiectelor comerciale
corespunde normativelor în vigoare cu condiţia modernizării şi dotării edilitare
corespunzătoare. Pentru obiectivele din sfera alimentaţie publică se prevede sporirea
numărului de locurilor cu 400, cu amplasarea uniformă a obiectivelor în locurile de
agrement, centrele comerciale etc.
Suprafaţa totală a pieţelor comerciale constituie 36 mii m 2 , pentru perspectivă se
propune construcţia halei agroalimentare cu suprafaţa totală 1,0 mii m 2 .
Pe teritoriul oraşului capacitatea totală a reţelei hoteliere constituie 300 locuri,
ceea ce constituie 8 locuri la 1000 locuitori, pentru perspectivă se propune sporirea
numărului de locuri în hoteluri cu 180.
Din obiectele de utilitate publică, pentru perspectivă se propune construcţia
curăţătoriei chimice cu capacitatea 520 kg/albituri / schimb şi a băii publice pentru
200 locuri.



< Înapoi la [Cuprins](../../cuprins.md) | [6. Dezvoltarea infrastructurii sociale](../v1_cap6.md)