< Înapoi la [Cuprins](../../cuprins.md) | [6. Dezvoltarea infrastructurii sociale](../v1_cap6.md)

# 6.3. Obiective cultură, sport şi agrement #
Obiectele de cultură, sport şi agrement au scopul de a satisface necesităţile
populaţiei pentru petrecea timpului liber. Acestea sunt reprezentate prin Palate şi
case de cultură, biblioteci, muzee, teatre, zone amenajate pentru agrement etc.
Actualmente, în oraş funcţionează Palatul de cultură, cu capacitatea 1000 locuri
şi 51 angajaţi şi clubul "Bereşti" cu capacitatea 150 locuri şi 2 angajaţi.
Cinematograful Patria – 600 locuri.
Muzeul de istorie şi etnografie din or. Ungheni a fost înfiinţat în anul 1967,
profilul muzeului - istorie, etnografie, instituţia dispune şi de o colecţie de artă
(pictură, grafică, sculptură, ceramică). Actualmente, muzeul are un patrimoniu de37
peste zece mii de exponate, grupate în colecţii de arheologie, port popular, scoarţe şi
covoare, ţesături tradiţionale, ustensile de uz casnic, numismatică, fotografii, cărţi
vechi şi documente, artă. Muzeul de Istorie şi Etnografie din Ungheni este unica
instituţie de profil din regiune care editează o publicaţie proprie – buletinul
"Pyretus!" aflat la a doua ediţie.
Biblioteca publică raională cu patru filiale şi biblioteca pentru copii cu
capacitatea 97,5 mii volume.

Pentru perspectivă, conform calculelor se prevăd un şir de activităţi:
1sporirea capacităţii instituţiilor de cultură cu 1000 locuri;
2înnoirea şi modernizarea stocului de carte în biblioteci – cu 70 mii volume,
3sporirea capacităţii cinematografului cu 400 locuri, conform exigenţelor în
vigoare;
4crearea centrului de promovare a meşteşugăriturilor şi tradiţiilor locale şi
regionale, prin amenajarea unui mini-sat de vacanţă în cadrul Parcului
etnografic;
5renovarea şi modernizarea muzeului de istorie şi etnografie;
6restaurarea edificiilor şi monumentelor de importanţă istorică şi culturală.
Oraşul Ungheni dispune de un potenţial turistic valoros cu landşaft pitoresc şi
condiţii naturale favorabile care contribuie la crearea condiţiilor optime pentru
odihnă, atît pentru turiştii în tranzit, cît şi pentru locuitorii oraşului. Spre regret,
actualmente, oraşul nu dispune de zone de agrement amenajate.
Ca urmare, pentru perspectivă se propune amenajarea scuarurilor în oraş,
reabilitarea parcului central, amenajarea plajei în preajma lac. Delia.

## Obiective de sport ##
Obiective de sport care în majoritatea cazurilor sunt amplasate pe teritoriul
instituţiilor de învăţământ şi sunt reprezentate prin stadioane cu suprafaţa totală 2,15
ha, terenuri sportive - 10 ha şi săli sportive - 12 345 m 2 .
Concomitent, în oraş este amplasat stadionul orăşenesc cu suprafaţa 0,8 ha
pentru 500 locuri şi sala de sport cu suprafaţa podelei 512 m 2 pentru 50 locuri.
Pentru asigurarea locuitorilor cu obiecte sportive pe perioada de calcul se
prevede modernizarea obiectivelor existente cu aducerea până la parametrii în
vigoare. Ca urmare, se preconizează construcţia complexului sportiv cu organizarea
zonei sportive, construcţia şi amenajarea terenurilor sportive şi a stadioanelor cu
suprafaţa totală 32,6 ha, sălilor sportive cu 3500 m 2 , bazinelor de înot – 1400 m 2
oglinda apei.
Pentru organizarea odihnei copiilor din raion şi a celor din oraş în perioada
estivală activează Tabără de odihnă "Plus Armonie" cu capacitatea 130 locuri şi 30
cadre didactice.


< Înapoi la [Cuprins](../../cuprins.md) | [6. Dezvoltarea infrastructurii sociale](../v1_cap6.md)