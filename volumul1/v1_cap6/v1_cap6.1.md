< Înapoi la [Cuprins](../../cuprins.md) | [6. Dezvoltarea infrastructurii sociale](../v1_cap6.md)

# 6.1. Instituţii de educaţie #
## Instituţii preşcolare #

Actualmente pe teritoriul or. Ungheni funcţionează 6 grădiniţe de copii cu
capacitatea totală 1695 locuri, de facto frecventează 1872 copii, ceea ce constituie
110% din capacitatea totală a instituţiilor preşcolare. La 1000 locuitori revin 44
locuri. Numărul personalului angajat constituie 290 persoane, inclusiv 123 cadre
didactice. La 1 educator revin – 15 copii.

Pentru normarea optimală, capacitatea instituţiilor preşcolare se determină prin
suportul structurii demografice a populaţiei, din calcul 85% din copiii grupei de vîrstă
corespunzătoare, de la 2 pînă la 6 ani inclusiv.
Conform calculelor pentru perioada de proiect, către anul 2030, se propune
sporirea capacităţii instituţiilor preşcolare cu 500 locuri, cu amplasarea instituţiilor în
zonele rezidenţiale noi.

## Instituţiile şcolare ##
Conform datelor prezentate de Direcţia învăţămînt din or. Ungheni actualmente
în oraş îşi desfăşoară activitatea 6 instituţii de învăţămînt primar şi secundar
profesional, inclusiv 5 licee teoretice şi o şcoală primară. Capacitatea totală a
instituţiilor de învăţămînt constituie 5892 elevi, de facto frecventează 4568 elevi,
ceea ce constituie 80% din capacitatea totală. La 1000 locuitori revin 154 elevi.
Numărul scriptic al angajaţilor - 448, inclusiv 311 cadre didactice.

În corespundere cu normele urbanistice pentru calcularea capacităţii instituţiilor
şcolare se evidenţiază 100% din copiii cu vîrsta 6 – 15 ani şi 75% cu vîrsta 16 – 17
ani.
Reieşind din cele menţionate, se poate de constatat că capacitatea instituţiilor
şcolare existente în oraş este suficientă, atît pentru perioada actuală cît şi pentru
perioada de calcul. Pentru perspectivă se propune, reparaţia şi modernizarea clădirilor
în corespundere cu normativele în vigoare.

## Instituţii de învăţământ secundar profesional, colegii şi instituţiile de învăţământ universitar ##

Oraşul este un important centru administrativ, cultural şi de pregătire a cadrelor
din centrul republicii. În oraş activează 3 colegii şi o şcoală profesională care
pregătesc specialişti pentru diferite ramuri ale economiei naţionale.
Colegiul de Medicină Ungheni îşi desfăşoară activitatea din 1992 după
evacuarea "Şcolii de Medicină din Bender" în oraşul Ungheni. Misiunea de bază a
colegiului este formarea cadrelor medicale, organizarea procesului de instruire în
corespundere cu standardele şi cerinţele naţionale a Organizaţiei Mondiale a
Sănătăţii, propagarea cunoştinţelor sanitaro-igienice în rîndul populaţiei etc.

Actualmente în colegiu activează 3 catedre: Catedra obiectelor clinice, Catedra
obiectelor preclinice, Catedra ştiinţelor socio-umane.
Pentru elevi sunt create condiţii optime de activitate: cabinete şi laboratoare
dotate cu utilaj, sală de calculatoare, sală de sport, cantină, căminul studenţesc.
Colegiul Agroindustrial pregăteşte specialişti pentru diferite ramuri ale
economiei naţionale. Elevii îşi fac studiile la următoarele specialităţi - mecanizarea
agriculturii, agronomie, merceologie, contabilitate, tehnologia păstrării şi prelucrării
fructelor şi legumelor.

Colegiul Naţional al Poliţiei de frontieră din Ungheni este unica instituţie de
acest fel în republică, iar pentru or. Ungheni reprezintă "Cartea de vizită a oraşului".
Colegiul pregăteşte specialişti-inspectori.
De asemenea în or. Ungheni
activează
Şcoala
Profesională
constituită în anul 1982, fiind prima
şcoală de acest gen din oraşul Ungheni.
Misiunea de bază a şcolii este
pregătirea
specialiştilor
pentru
economia naţională care să corespundă
standardelor europene.
În prezent, la Şcoala profesională
din Ungheni activează şapte catedre - vînzători de produse nealimentare şi
alimentare, zugravi-tencuitori, electrogazosudori-montatori, lăcătuşi la reparaţia
automobilelor, electromontori la repararea şi întreţinerea utilajului electric, tîmplari,
operatori la calculatoare, cusătorese.
Pentru perspectivă se propune deschiderea unei instituţii de învăţămînt superior
cu profil sportiv.

## Instituţiile extraşcolare ##
Instituţiile extraşcolare contribuie la dezvoltarea multilaterală a copiilor.
Actualmente în oraş activează şcoala muzicală unde sunt încadraţi 240 copii şi 30
cadre didactice şi şcoala de arte plastice unde sunt încadraţi 190 copii şi 9 profesori.

< Înapoi la [Cuprins](../../cuprins.md) | [6. Dezvoltarea infrastructurii sociale](../v1_cap6.md)