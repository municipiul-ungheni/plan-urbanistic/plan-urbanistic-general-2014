< Înapoi la [Cuprins](../../cuprins.md) | [5. Fondul locativ](../v1_cap5.md)

# 5.2 Reglementări #
Structura deja formată a fondului locativ în oraşul Ungheni este prezentată
printr-un spectru înalt a genului şi tipului de clădiri multietajate. În prezent practic se
construiesc doar construcţii individuale. În perioada anilor 2008-2013 au fost date în
exploatare 104 case individuale cu suprafaţa totală 10,8 mii m 2 . Pentru această
perioadă, darea în exploatare a caselor – bloc multietajate nu s-a înregistrat. Aceasta
duce la pierderi din rezervele teritoriului oraşului, la comprimarea reţelei de stradă-
drum, reţelei tehnico-edilitare, creează disfuncţionalităţi în asigurarea populaţiei cu
transport public.
Se recomandă de a dezvolta două sisteme de construcţie de bază:
- construcţia individuală a caselor cu 1-2 nivele cu loturi pe lângă casă, pentru
construcţia compactă a formaţiunilor locative 25-30 oam/ha;
- construcţia blocurilor locative cu 5-9 nivele, pentru construcţia compactă 250-
300 oam/ha.
Volumul construcţiilor noi pentru perioada de perspectivă se va extinde cu –
150,6 mii m2 suprafaţă totală. De asemenea conform proiectului se preconizează
următoarea structură locativă cu aspect constructiv: 1-2 nivele cu lot pe lângă casă –
20% din volumul total al fondului locativ racordat, sau 30,1 mii m 2 din suprafaţa
totală şi 80% sau 120,5 mii m 2 din suprafaţa totală – blocuri locative multietajate.
Construcţia caselor locuibile individuale, unietajate, se preconizază în sectorul
Bereşti (suprafaţa teritoriului 7 ha) şi Dănuţeni (suprafaţa teritoriului 23 ha).
Construcţia caselor-bloc multietajate se prevede în zona Centru a oraşului (suprafaţa
teritoriului 2 ha) şi sectorul Tineretului (suprafaţa teritoriului 14 ha).
Volumul construcţiilor noi se stabileşte reieşind din evaluarea dinamicii
dezvoltării fondului locativ pentru perioada de retrospectivă, resursele teritoriale a
oraşului şi numărul populaţiei pentru perspectivă.
Volumul total al fondului locativ pentru perioada de proiect va constitui 800 mii
2
m suprafaţă totală, inclusiv: case cu 1-2 nivele cu lot pe lângă casă cu suprafaţa
264,7 mii m 2 şi blocuri locative multietajate cu suprafaţa 535,3 mii m 2 , ca urmare se
preconizează sporirea asigurării medii cu locuinţe pînă la 20m 2 /loc.
Calculul fondului locativ pe formaţiuni locuibile a sectoarelor şi repartizarea
populaţiei pentru perspectivă – a. 2028, este reflectat în tab. 5.2.1; 5.2.2; 5.2.3.

Dotarea şi întreţinerea sistemelor de alimentare cu apă, canalizare, încălzire
individuală este o condiţie obligatorie atît pentru construcţii noi cît şi pentru fondul
locativ existent, reconstruit şi modernizat conform normativelor în vigoare.
Din volumul total al construcţiilor noi preconizate, este necesar de a se prevedea
construcţia locuinţelor sociale – 3-5 % din construcţia caselor –bloc multietajate, ceea
ce va constitui 6 mii m 2 .
Unul din factorii principali de formare şi dezvoltare a fondului locativ este
asigurarea, păstrarea şi reînnoirea fondului locativ existent din contul reparaţiei
capitale, reconstruirea şi modernizarea clădirilor.
Folosirea în construcţie a noilor tehnologii şi materiale, va permite de a crea
construcţii originale şi moderne a localităţilor populate, atrăgătoare din punct de
vedere arhitectural.
Pentru perspectivă se preconizează sporirea nivelului de dotare a fondului
locativ cu apeduct, canalizare şi alimentare cu căldură, implementarea sistemului
individual de încălzire a locuinţelor, ceea ce va contribui la sporirea confortului de
abitaţie a populaţiei oraşului.

Indicii de bază a dezvoltării fondului locativ Ungheni


< Înapoi la [Cuprins](../../cuprins.md) | [5. Fondul locativ](../v1_cap5.md)