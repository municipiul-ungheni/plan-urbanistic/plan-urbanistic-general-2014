< Înapoi la [Cuprins](../../cuprins.md) | [5. Fondul locativ](../v1_cap5.md)

# 5.1. Situaţia existentă #

Conform datelor statistice de la 01.01.2013 suprafaţa totală a fondului locativ în
or. Ungheni constituie 649,4 mii $m^2$ fiind, reprezentat în deosebi de case cu 1-2 nivele
cu lot pe lîngă locuinţă, clădiri-bloc cu 2-3; 4-5 şi 5-9 nivele. Fondul total de locuinţe
în oraş este de 12719 case/apartamente, din care 4231 case individuale cu lot pe lîngă
casă şi 8488 apartamente în clădiri - bloc. Fondul de locuinţe în or. Ungheni în
perioada anilor 2004-2013 a sporit de la 640,0 mii $m^2$ pînă la 649,4 mii $m^2$ . Dinamica
fondului de locuinţe este prezentată în tab. 5.1.1

Dinamica fondului de locuinţe (supr. mii $m^2$)


Suprafaţa medie a unei locuinţe constituie 51,1 $m^2$ .

Fondul de locuinţe al or. Ungheni se extinde pe o suprafaţa de 664 ha, densitatea
populaţiei constituie 58,0 loc/ha.
Zona locativă a oraşului este divizată în 6 microraioane (sectoare), care sunt
caracterizate prin următoarele aspecte:
Bereşti - suprafaţă totală 58,8 mii $m^2$ , reprezenta în deosebi prin case cu 1-2
nivele cu lot pe lîngă casă. În partea de vest a microraionului este amplasat cartierul
cu construcţii mai vechi amplasate haotic, în partea de est - cu construcţii moderne
sistematizate.
Tineretului - microraion cu clădiri noi cu multe apartamente cu 5-9 nivele,
suprafaţa totală 214,3 mii $m^2$ .
Centru - ansamblul de locuinţe al acestui sector a stat la baza formării oraşului,
suprafaţa fondului de locuinţe 184,4 mii $m^2$ , din care 30,61 mii $m^2$ case individuale cu
lot pe lîngă casă şi 154,23 mii $m^2$ cladiri-bloc, din care 135,3 mii $m^2$ clădiri mai mult
de 5-9 nivele;
Ungheni-Vale - suprafaţa totală 27,8 mii $m^2$ , reprezenta în deosebi prin case cu
1-2 nivele cu lot pe lîngă casă.
Ungheni-Deal - suprafaţa totală 18,8 mii $m^2$ fiind reprezentat totalmente cu
case individuale cu lot pe lîngă casă.
Dănuţeni - suprafaţa totală a fondului locativ 144,87 mii $m^2$ , este prezentat prin
case individuale cu lot pe lîngă casă (73 %), clădiri - bloc cu 2-3 nivele suprafaţa
totală 2,65 mii $m^2$ , clădiri - bloc cu 4-5 nivele suprafaţa totală 35,93 mii $m^2$ .
Fondul locativ divizat pe sectoare este reflectat în tab.5.1.4.

Casele locative construite în perioada anilor 1950-1970, suprafaţa totală 61,0 mii
m2 necesită reconstrucţie şi reparaţie capitală.



< Înapoi la [Cuprins](../../cuprins.md) | [5. Fondul locativ](../v1_cap5.md)