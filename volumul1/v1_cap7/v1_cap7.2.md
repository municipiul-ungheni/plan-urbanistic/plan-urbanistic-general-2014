< Înapoi la [Cuprins](../../cuprins.md) | [7. Turism](../v1_cap7.md)

## 7.2. Dezvoltarea turismului or. Ungheni ##

La etapa actuală turismul este slab dezvoltat nu doar în oraşul Ungheni, dar şi
în republică, din cauza lipsei sistemului de dezvoltare în sfera turismului, cât şi lipsa
unui program complex de dezvoltare a business-ului turistic şi susţinerea din partea
statului.
Oraşul Ungheni nu este în concordanţă cu piaţa internaţională, chiar dacă are
multe resurse cu valoare potenţială care , fiind valorificate, ar putea trezi interesul
turiştilor de peste hotare. Chiar şi aşezarea geografică a oraşului Ungheni constituie o
oportunitate pentru promovarea valorilor turistice.
Pentru realizarea programului de dezvoltare a turismului în oraş este oportun în
primul rând de a efectua reconstrucţia monumentelor de cultură, istorie, arhitectură,
reparaţia drumurilor auto, construcţia hotelurilor. Ca urmare în or. Ungheni se
propune crearea centrelor de informare turistică, instalarea panourilor informative şi
indicatoarelor. Începînd cu anul 2000, oraşul Ungheni găzduieşte Tabăra de Sculptură
Ungheni, care se desfăşoară bienal în lunile august – septembrie şi la care participă
sculptori profesionişti din ţară şi de peste hotare, lucrările cărora sunt amplasate în
aer liber de-a lungul străzilor principale. Pentru dezvoltarea continuă şi promovarea
acestui eveniment cu potenţial turistic, se propune crearea unui centru a
meşteşugurilor şi tradiţiilor locale şi regionale.
Pentru perspectivă, în conformitate cu prevederile stipulate în proiectul "PUG
Ungheni 2015-2030" se prevede amenajarea unei zone turistice de-a lungul r. Prut,
valea Prutului de Mijloc fiind una din cele mai pitoreşti văi din spaţiu carpato-
danubiano–pontic, cu Toltrele Prutului care sunt un fenomen unicat în Europa cu
rezervaţiile ştiinţifice Pădurea Domnească şi Plaiul Fagului.
În scopul sporirii atractivităţii oraşului şi promovarea lui la nivel internaţional ,
se propune amenajarea unui mini-satuc de vacaţă în parcul din preajma Palatului
Culturii. De asemenea, se propune amenajarea şi reabilitarea zonelor de agrement,
parcurilor, plajelor existente , curăţarea bazinelor acvatice.
Oraşul Ungheni păstrează memoria vestitului arhitect francez Alexandre
Gustave Eiffel, care a construit podul de cale ferată peste Prut în anul 1877. Podul a
intrat în istorie sub numele de Podul de flori : la 6 mai 1990, zeci de mii de români de pe
cele două maluri ale Prutului s-au reîntîlnit după circa jumătate de secol de
înstrăinare. Deşi este de o frumuseţe rară, acesta nu poate fi admirat de turişti, fiindcă
zona nu este amenajată corespunzător, în plus, este amplasat în zona de frontieră.
Pentru dezvoltarea turistică a acestei regiuni, se propune amenajarea şi organizarea
teritoriului din preajma podului, crearea unei zone şi dotarea cu elemente şi
echipamente moderne ce ar facilita accesul turiştilor să admire priveliştea creată de
acest monument architectural .
Pentru crearea infrastructurii turistice moderne se propun următoarele măsuri:
- implementarea mecanismelor economice privind stimularea dezvoltării turismului
intern şi atragerea investiţiilor în acest domeniu;
- dezvoltarea infrastructurii turismului în corespundere cu cerinţele internaţionale;
- crearea condiţiilor favorabile pentru cooperare reciprocă între structurile turistice,
bancare, hoteliere, de transport şi agenţiile de asigurare în scopul organizării rutelor şi
complexelor turistice în baza implementării tehnologiilor moderne şi dezvoltarea
bazei tehnico-materiale a acestei ramuri;
- integrarea pe piaţa republicană şi internaţională a turismului şi dezvoltarea
cooperării internaţionale în domeniul dat;
- asigurarea procesului de promovare a informaţiei şi reclamarea producţiei turistice
pe piaţa internă şi externă;
- modernizarea sistemului de pregătire, reinstruire şi reprofilare a cadrelor din sfera
turismului;
- organizarea şi susţinerea activităţilor culturale; tradiţiilor şi sărbătorilor
naţionale;
- construcţia obiectivelor infrastructurii turistice (hoteluri, kamping, motel).44
Realizarea acestor prevederi este posibilă în condiţiile respectării următoarelor
principii:
- planificarea, amenajarea si exploatarea turistica, la nivel local, ca parte
integranta a strategiei dezvoltării durabile a turismului la nivel naţional;
- participarea diferitelor autorităţi publice, a sectorului privat, a asociaţiilor de
protecţie a mediului şi a populaţiei la procesul de planificare a turismului;
- gestionarea şi planificarea durabilă a turismului, ţinându-se cont de protecţia
mediului natural şi uman în zonele de primire;
- informarea, educarea, încurajarea şi atragerea populaţiei locale în procesul de
amenajare turistică.

< Înapoi la [Cuprins](../../cuprins.md) | [7. Turism](../v1_cap7.md)