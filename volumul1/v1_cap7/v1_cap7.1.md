< Înapoi la [Cuprins](../../cuprins.md) | [7. Turism](../v1_cap7.md)

# 7.1. Situaţia existentă #
Activizarea activităţii în ramura dată este prioritatea dezvoltării socio-economice
a oraşului. Sarcina principală a reorganizărilor din acest domeniu constă în crearea
complexului turistic contemporan eficient şi competitiv, care va fi capabil să asigure,
pe de o parte posibilităţile vaste privind satisfacerea necesităţilor cetăţenilor
republicii şi turiştilor străini prin oferirea unui spectru larg de servicii turistice, iar pe
altă parte, va contribui la dezvoltarea economiei oraşului prin sporirea numărului de
locuri de muncă şi fluxul valutei străine în buget, conservarea şi utilizarea raţională a
patrimoniului cultural şi natural.
Potenţialul turistic al oraşului şi zona de influenţă a lui sunt foarte atractive, însă
aceste posibilităţi sunt valorificate şi utilizate la minim având predominant, caracter
de tranzit.
Teritoriul or. Ungheni face parte din zonele favorabile ale republicii pentru
organizarea turismului şi odihnei populaţiei. Peisajele şi landşaftul pitoresc, prezenţa
r. Prut, monumentelor de istorie, arhitectură şi cele naturale redau oraşului un aspect
unic şi neimitabil. Pe teritoriul oraşului Ungheni sunt amplasate un şir de monumente
de istorie, arhitectura şi cultură, cele mai valoroase obiective din oraş sunt: Biserica
"Sf. Alexandru Nevschi", "Sfîntul Ierarh Nicolae", Podul feroviar peste rîul Prut
"Eiffel", "Muzeul de Istorie şi Etnografie". De asemenea oraşul este cunoscut prin
"Aleea de castani".
Ungheniul este un important centru administrativ, economic şi cultural şi
dispune de un potenţial valoros pentru dezvoltarea turismului şi în special a
turismului cultural. De-a lungul istoriei aici au avut loc multe evenimente istorice şi
însăşi oraşul a dat naştere multor personalităţi marcante, activitatea cărora face parte
din patrimonial cultural internaţional.
Oraşul Ungheni este gazda numeroaselor evenimente culturale cum ar fi –
"Zilele oraşului Ungheni", "Sărbătoarea Recoltei şi Promovării Tradiţiilor Naţionale",
Festivalul raional al obiceiurilor şi tradiţiilor de iarnă „După datina străbună”,
Festival folcloric "Efim Junghietu", Festivalul “Mărţişor”, "Tabăra de Sculptură
Ungheni", "Târgul Naţional de Ceramică".
De asemenea, oraşul dispune de condiţii de dezvoltare a turismului industrial
datorită prezenţei întreprinderilor industriale unice în ţară şi bazate pe tradiţii
populare în domeniul ţesutului
covoarelor, producerea articolelor de
ceramică, producerea vinurilor,
activităţi întruchipate în activitatea
Fabricii de covoare Ungheni,
Fabricii de ceramică - unica de acest
fel in ţară şi Fabrica de vinuri.

## Podul Eiffel ##
Podul Eiffel este un pod metalic peste Prut dintre or. Ungheni (RM) şi comuna
Ungheni (România). Podul a fost proiectat şi construit de Gustave Eiffel, care fusese
invitat la Ungheni de către direcţia căilor ferate din Basarabia.
Numele oficial a fost adoptat la 20 aprilie 2012, în cadrul Şedinţei Consiliului
orăşenesc Ungheni.
Podul a intrat în istorie sub numele de Podul de Flori la 6 mai 1990 cînd zeci de
români de pe ambele maluri ale Prutului s-au reîntîlnit după cca. jumătate de secol de
înstrăinare.

## Biserica „Sf. Alexandru Nevski” ##
Biserica a fost întemeiată la 30 august 1903, iar în iunie 1959 biserica a fost
închisă, în anul 1967 în incinta bisericii a fost deschis muzeul de istorie şi etnografie
din Ungheni. În anul 1968 s-a reuşit declararea bisericii „monument de istorie şi
arhitectură de importanţă naţională”. În luna mai 1989 lăcaşul a fost redeschis.
În anul 2011 prin decizia Sfîntului sinod al Mitropoliei Moldovei biserica a fost
transformată în catedrală episcopală.

## Biserica „Sfîntul Ierarh Nicolae” ##
Monument ecleziastic şi de istorie, ridicată în anul 1882 , de către prinţul
Constantin Moruzi . În cavoul acestei biserici este înmormîntat ctitorul ei.
Arhitectura edificiului impresionează şi astăzi, iar planul în formă de cruce şi
acoperişul cu 9 turle de diferite dimensiuni amintesc de stilul rusesc, în vogă pe
atunci.
În anul 1961 biserica a fost închisă timp de cîteva decenii. Cele mai valoroase
lucruri sfinte au dispărut din biserică, însă cîteva icoane şi iconostasul s-au păstrat
pînă în prezent.42
Biserica a fost redeschisă la începutul anilor 90 ai secolului trecut, şi cu aportul
enoriaşilor a fost reparată. Unele elemente din arhitectura originală însă nu au mai
putut fi restaurate.

## Aleea de castani ##
Aleea de castani din oraşul Ungheni se numără printre primele zece cele mai
lungi alei din Europa şi a devenit un obiectiv turistic, mai ales, pentru cetăţenii străini
care vizitează R. Moldova. Pe o lungime de circa cinci kilometri au fost plantaţi
peste 650 de arbori de castani. Copacii au fost plantaţi în anul 1975 şi au devenit deja
un obiectiv turistic bine cunoscut.



< Înapoi la [Cuprins](../../cuprins.md) | [7. Turism](../v1_cap7.md)