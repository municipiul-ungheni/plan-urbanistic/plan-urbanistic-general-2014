< Înapoi la [Cuprins](../../cuprins.md) | [4. Populația](../v1_cap4.md)

# 4.2 Reglementări #

Calculul numărului populaţiei pentru perspectivă se bazează pe datele dinamicii
numărului populaţie, structurii demografice, indicilor mortalităţii, natalităţii şi
migraţiei populaţiei ce au avut loc în perioada de retrospectivă.
În dependenţă de intensitatea schimbărilor prevăzute în dezvoltarea socio-
economică a oraşului calculul numărului populaţiei s-a efectuat în 3 variante -
optimistă, medie şi pesimistă. Indicii de ieşire a calculului numărului populaţiei
reflectă prognoza dezvoltării socio-economice optime şi corespund variantei trei.
Luând în consideraţie multiplii indici şi acţiunile stipulate în planul urbanistic
general al oraşului ce depinde de structura demografică a populaţiei, în baza
prognosticului numărul populaţiei din localitate este acceptată metoda „permutării
vîrstelor” cu utilizarea datelor perioadei de retrospectivă 2004-2013.
Cercetările efectuate relevă dinamica structurii populaţie pe sexe şi vîrstă,
procesele naturale de întinerire şi migrare a populaţiei.
Argumentarea acestei abordări, cu utilizarea modelului matematic, se bazează pe
prioritatea folosirii coeficientului progresiv pentru localităţile cu numărul populaţiei
de la 25-50 mii locuitori şi statut de centru raional. Aceasta se confirmă prin calcule
conform formulei Pn=Po(1+k/1000) n, unde;
Pn – numărul calculat al populaţiei;
Po – numărul de bază a populaţiei (38,2 mii oam.)
k - coeficientul acceptat a sporului total al populaţiei (2,0 promile)
n - perioada etapei prognozate - 15 ani.
Conform calculelor de prognoză efectuate prin metoda "permutării vârstelor"
către anul 2028 numărului populaţiei va constitui – 39,9 mii oameni.
Tabelele de calcul al matricelor şi variantele de calcul al numărului populaţiei
pentru perspectivă se anexează.

Dinamica numărului populaţiei or. Ungheni pentru perspectivă (mii locuitori)



< Înapoi la [Cuprins](../../cuprins.md) | [4. Populația](../v1_cap4.md)