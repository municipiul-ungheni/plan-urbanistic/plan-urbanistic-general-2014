< Înapoi la [Cuprins](../../cuprins.md) | [4. Populația](../v1_cap4.md)

# 4.1. Situaţia existentă #
Conform datelor statistice la 01.01.2013 populaţia or. Ungheni constituia
38,2 mii locuitori, sau 2,5% din numărul total al populaţiei urbane al republicii.

Cea mai mare densitate a populaţiei se înregistrează în sectoarele cu clădiri
multietajate - Tineretului şi Centru.
Dinamica principalelor parametri demografici pentru perioada de retrospectivă

Reieşind din cercetările efectuate, principalele schimbări demografice începând
cu anul 2004 sunt:
- intesificarea proceselor migraţioniste a populaţiei în ultima perioadă
influenţată de şomaj, condiţii de abitaţie nesatisfăcătoare ale unor grupuri ale
populaţiei, venituri modeste în majoritatea familiilor care nu asigură nivelul minim de
trai;
- schimbările în structura pe vârste ale populaţiei, sporirea cotei populaţiei peste
vârsta aptă de muncă şi reducerea cotei populaţiei până la vârsta aptă de muncă.
Cu toate că, coeficientul sporului natural al populaţiei în perioada anilor 2004-
2013 a înregistrat valori pozitive, pericolul schimbărilor negative în procesul de
planificare a sporirii numărul populaţiei mai persistă.

< Înapoi la [Cuprins](../../cuprins.md) | [4. Populația](../v1_cap4.md)