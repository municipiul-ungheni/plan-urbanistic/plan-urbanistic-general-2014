< Înapoi la [Cuprins](../cuprins.md)
# 12. Concluzii. Măsuri de continuare #

Analizînd cele spuse şi prezentate în corelare cu opţiunile farurilor
administrative locale ale tuturor sectoarelor oraşului Ungheni precum şi doleanţele
populaţiei, putem concluziona în mare măsură asupra posibilităţilor de dezvoltare a
oraşului:
- Este de dorit utilizarea la întreaga capacitate a potenţialului industrial existent,
reanimat şi reamplasat – ce va posibilita majorarea numărului de angajaţi în acest
sector al economiei;
- Se va majora prin extindere zona spaţiilor verzi, în deosebi în lunca inundabilă
prin valorificarea noilor teritorii;
- Se va reînoi şi majora reţeaua de străzi şi drumuri, va apărea străzi amenajate
noi, care va ameliora mult situaţia transportului urban şi mai cu seamă descărcarea
străzilor principale din zona centrală a oraşului;
- În noua zonă de odihnă şi agrement din valea r. Prut va fi construit un stadion
raional şi alte obiecte de sportj;
- Se va ameliora sistemul de parcare a autoturismelor de scurtă şi lungă durată;108
- Pe o treaptă superioară se va ridica procesul de comercializare cît şi de
deservire în integral a populaţiei;
- Populaţia se va bucura de îmbunătăţirea condiţiilor de trai.
Există posibilitatea de desfăşurarea activităţii de proiectare prin noi studii de
specialitate cum ar fi:
Elaborarea proiectelor de specialitate în baza unor PUZ-uri şi Pud–uri, care vizează
rezolvarea unor probleme concrete, ce apar pe teren.
Pentru o bună aplicare a prezentei lucrări se vor lua măsuri de definitivare a dreptului
de proprietate conform legii fondului funciar. Măsurile de rezervare a terenurilor
pentru obiective de interes public vor avea de asemenea în vedere prevederile legii
privind exproprierea pentru cauza de utilitate publică.
Prezenta lucrare va fi prezentă la şedinţa consiliului Primăriei or. Ungheni după
adunarea tuturor coordonărilor cu organele locale publice şi de stat cointeresate,
pentru a fi discutat şi aprobat.
Odată aprobat Planului Urbanistic General cît şi Regulamentul local de urbanism
aferent, capătă valoare juridică, fiind operabil între organele administrative şi de Stat,
ce urmăresc aplicarea lui şi diverşii solicitanţi ai autorităţilor de construire.

< Înapoi la [Cuprins](../cuprins.md)