< Înapoi la [Cuprins](../cuprins.md)
# 2. Situația hidrogeoconstructivă #

Divizarea teritoriului municipiului Ungheni după condițiile geotehnice
(aprecierea favorabilității terenului pentru construcții)

## Poziția fizico-geografică a orașului Ungheni ##
Teritoriul or Ungheni coincide cu versantul de stînga a văii râul Prut și este fragmentat de rețeaua de vîlcele și ravene a văii râul Delia. Pe râul Prut ambele văi se află în partea de vest și nord -vest a orașului, ocupînd teritorii limitate. Lățimea albiei - 30-40 m, meandrează puternic și în unele locuri devierii de la lunca deasupra terasei se află lunca înaltă și joasă. Cota maximă aluncii 36,8 - 38,3 m. 

Suprafața luncii este parțial înmlăștinită, înclinată către albie, fragmentată de albia veche, ocupată cu iazuri și lacuri înămolite. Cea mai extinsă albie veche al bazinelor acvatice ocupă suprafața 14 ha - între rîu și construcțiile din zona Berești. Prezența bazinelor acvatice influențează procese de subinundate a teritoriului lunca râul Prut.

Alte teritoriii din luncă sunt ocupate cu construcții, grădini, spații verzi, instalații tehnice și alte măsuri inginerești: inclusiv îndiguirea albiei r.Prut împotriva viiturilor Pe râul Delia lunca înaltă și joasă parțial este ocupată de apele lacului artificial Delia. La gura de vărrsare a râul Delia în râul Prut o parte extinsă a luncii înalte și joase (în regiunea autodromului strada Ungheni) cu cotele de înălțime pînă la 37,50 m prevalează astfel de particularități geomorfologice ce coincid cu segmentul r.Prut.

Prezența elementelor rețelei fluviale (rîulețe, albii vechi, suprafața teraselor luncilor) actualmente sunt modificate de construcțiile intense.

În rezultatul inundării văii râul Delia de bazinul de acumulare, restul luncii se observă ca o fîșie îngustă, în deosebi pe partea dreaptă a văii lunci. Fîșia cu cotele de înălțime 40,8 - 41,9 m dintre iazuri și treapta terasei a doua este înămolită, complicată de terasamentul căii ferate.

Luncă coincide cu baza terasei a doua la nivelul 41-42 m. În amontele suprafeței de apă a iazului terasei deasupra luncii râul Delia cu cotele de înălțime 40,8 - 41,5 m, pînă la st. Unțești sunt puternic înmlăștinite, acoperite cu stuf.

O parte din albia rîului este artificial îndreptată. la suprafața terasei, paralel cu albia nouă sunt construcite canale pentru irigație cu lungimea pînă la 1 km și lățimea pînă la 4 m, actualmente cu grad divers de înămolire. Depunerile contemporane de nămol se formează în această parte cu ape mici a lacului.

Lunca afuentului de stînga a râul Delia - este plană, fără înlinare cu cotele de înălțime 41.3 - 45.0 m.

Rîulețul are albia plană, puțin meanrdrată, adîncimea eroziunii (0,3-0,5 m), lățimea 1-3 m.

Alt afluent al râul Prut situat în limitele zonei construite a orașului este râul Băilești. Cota de înălțime a suprafeței luncii - 37,0 m.

### Prima terasă de-asupra râul Prut ###
Suprafața dezvoltării coincide cu cele mai mari meandre a rîului. Cea mai lată terasă se înregistrează în partea de vest a orașului. Suprafața de răspîndire a ei coincide cu subzona I.

Cota I terase de-asupra luncii în locul confluenței cu terasa II constituie 39,0 - 41,0 m. Toată suprafața terasei este valorificată intens. Elementele morfologice se observă neclar. Foarte intens este valorificată treapta ocupată cu construcții individuale . Pe alocuri treapta terasei este deschisă de carierele subterane.

### A doua terasă de-asupra luncii râul Prut ###
Ocupă partea principală a teritoriului orașului, între râul Prut și valea r-lui Delia. Dinspre est suprafața terasei este limitată de treptele Codrilor de vest, iar în apropierea râul Prut este întreruptă de treapta cu înălțimea pînă la 20 m. În această regiune este posibilă dezvoltarea mai intensă a alunecărilor.

O mare parte a terasei este ocupată de clădirle industriale și locuibile.

Suprafața terasei, atît în hotarele orașului, cît și peste hotarele lui sunt practic orizontale cu înclinare locală nesemnificativă.

În hotarele orașului și localitățile din componența lui se întîlnesc debleuri mici, retezișuri, suprafețe terasate neuniform pentru construcții cu grad de compactare diversă a grundului. În partea centrală a orașului suprafața terasei a fost redusă semnificativ, în schimb a sporit suprafața cu acoperire din asfalt, beton, pietriș a fost amenajată scurgerea pluvială.

La periferia de sud a orașului, în regiunea uzinei biochimice, terasa este întretăiată de r-l Băilești, înclinarea pantei în limitele zonei construcite constitiuie 5-6&deg; .

De la r-l Băilești în direcția sud și sud-estică de la valea râul Delia, suprafața terasei este plană, cu panta de înclinare 1-3&deg; în direcția râul Prut și r-l Delia.

De la barajul lacului Delia în continuare de-a lungul afluentului de stînga a r. Delia (versantul de stînga a văii) înclinarea treptei constituie 6-9&deg;, înălțimea de la bază pînă la vîrf - 15-20m.

Suprafața versantului lipsită de construcții parțial este terasată și consolidată cu plantații de arbori și arbuști. Analiza condițiilor geologo-geomorfologice a versantului și rezultatul lucrărilor de forare din această regiune denotă lipsa proceselor de alunecări. Versantul se află în stare de stabilitate.

Pe unele segmente separate se observă dezvoltarea ravenelor. Lungimea ravenelor 10-20m, adîncimea 2-3m.

Treapta terasei în locurile de coincidență cu râul Prut, permanent, este supusă derodării - se dezvoltă procese de alunecări.

### Terasa a VI-a de-asupra râul Prut ###
Are răspîndire nesemnificativă și se manifestă peste hotarele orașului pe versantul de stînga a văii râul Delia. Ocupă partea de pînă la cumpăna apelor și cumpăna apelor pe treapta Codrilor de vest. 

Cota relativă a bazei terasei - 71,0m, se înregistrează înclinarea semnificativă a pantei suprafeței terasei în direcția versantului cu alunecări a râul Delia.

### Versanții afectați de alunecările străvechi ###
Pe ambele maluri ai râul Delia versanții sunt afectați de alunecările străvechi. Versantul de dreapta afectat de activitatea proceselor de alunecări străvechi afectează nesemnificativ teritoriul. 

Răspîndire mai vastă a alunecărilor străvechi se înregistrează pe partea stîngă a văii râul Delia, în amonte de iazul Delia. 

La hotarul versantului abrupt, care este împădurit și versanții mai domoli al terasei a șasea care este arată se observă ravena șerpuindă a văii de 2,5 km. În partea centrală, care este mai afectată, are forma bombată, ravena are înălțimea pînă la 5 m, la capete de la 2-3 m pînă la 1m. Mai sus pe versant de la ravenă se observă fisuri înerbate întrerupte. Cu 70-100 m mai sus de ravenă se începe zona de parc cu plantații multianuale. 

Suprafața cuprinsă între pădure și ravenă este acoperită de mocirle, stuf și altă vegetație. Apa este evacuată prin numeroasele ojașe și ravene. 

Izvoarele existente nu sunt captate,  vacuarea lor nu este sistematizată.

### Versanții alunecărilor active ###
Sunt răspîndiți în valea râul Delia și pe versantul de stînga a râul Prut. Tot versantul
afectat de alunecări se află peste hotarele zonei construite. În nemijlocita apropiere de
ravenă se observă debușarea apelor subterane în formă de izvoare. De-a lungul albiei
râul Prut se observă acțiunea intensivă, contemporană a rîului sub formă de eroziune
laterală și de fund. În continuare eroziunea fluvială se dezvoltă de-a lungul malurilor
și condiționează dezvoltarea proceselor de surpare - grohotiș și alunecări.
Particularitate caracteristică este si ruperea blocurilor mari de la versantul de bază
(argile nisipoase tasabile și argile) cu alunecarea ulterioară a părții de jos a blocului
și prăbușirea părți superioare.

Activizarea proceselor de alunecări este condiționată de activitatea erozională a
râul Prut, înaintarea rîului în treapta terasei unde procesele de alunecări sub acțiunea
eroziunii fluviale se vor dezvolta în continuare.

### Versanții eroziunii deluviale ###
Procesele eroziunii deluviale au loc pe treptele Codrilor de Vest și în valea r.
Delia. Geomorfologic sunt localizate între terasa a doua și a șasea a râul Prut. În partea
de sud-vest a regiunii Berești versantul eroziunii deluviale afectează nesemnificativ15
teritoriul examinat. Înclinarea pantei constituie 2-3 grade. Înălțimea versantul scade
în direcția vestică a terasei de-asupra luncii. Versantul este arat. Teritoriul nu este
afectat de ravene, vîlcele și gropi. Învelișul vegetal și stratul inferior humificat este
supus eroziunii. Alt segment afectat de eroziune este malul drept al râul Delia.
Versantul este ocupat de construcții - procese de eroziune sunt încetinite și nu au
manifestare activă.

## Structura geologică ##
În structura geologică a teritoriului examinat la adîncimea pînă la 20,0 m
participă următoarele depozite:
* neogene;
* neogene și ale cuaternarului inferior;
* cuaternarului inferior;
* cuaternarului mediu și superior;
* cuaternarului superior și contemporan;
* contemporane.

### Depozitele neogene - subetajul sarmațianului mediu ###
Depozitele sunt răspîndite pe tot teritoriul fiind acoperite de alte formațini,
adîncimea sedimentării 9,0 - 13,2 m de la suprafața terestră. Grosimea stratului
variază de la 1,0 - 9,5 m. Depunerile sunt prezentate prin argile de culoare gri-
cenuție, gri închis cu intercalații de nisipuri.

### Depozitele neogene și ale cuaternarului inferior ###
Sunt depozitate l aadîncimi diferite, care variază de la 5,0 - 13,0 m și constituie
baza terasei II de-a supra luncii. Depunerile sunt prezentate prin argile stratificate cu
intercalații de nisipuri și sol nisipo-lutos.

### Depozitele cuaternarului inferior ###
Sunt prezentate prin depunerile terasei a IV de-a supra luncii râul Prut. Grosimea
stratului - pînă la 10,0 m. Depunerile sunt prezentate prin sol nisipo-lutos, argilă
nisipoasă, nisipu cuarțos, intercalații de grund în amestec cu pietriș și prundiș.

### Depozitele cuaternarului mediu și superior ###
După geneză se referă la cele aluviale și împreună cu cele ale cuaternarului
superior sunt sedimentate pe terasa II de-asupra râul Prut. Grosimea stratului - pînă la
2m. Sunt prezentate prin grund în amestec cu pietriș și nisip care sunt sedimentate
pe argilele neogene.

### Depozitele cuaternarului superior și contemporan ###
Se disting 4 tipuri:
* ***depunerile aluviale-deluviale*** - în profilul geologic predomină argilele nisipoase macroporoase de culoare galbenă și moro cu nișe de nisipuri.

* ***depuneri alivionale*** - au răspîndire mai restrînsă, care aderă la albia râul Prut. La gura de vărsare a râul Prut și râul Delia pe argilele heogene sunt sedimentate nisipuri pulverulente cu intercalații de argile și sol nisipo-lutos precum și prundiș. Grosimea stratului - 14,5 - 16,8 m. Suprafața subiacentă este acoperită de nisipuri sau sol cu amestec de prundiș completat de nisipuri. Depozitele cuaternarului mediu și superior și cele ale cuaternarului superior sunt sedimentate pe terasa II de-asupra luncii și sunt deschise prin sonde în partea centrală și de sud a orașului.

* ***depunerile aluviale-deluviale ale cuaternarului superior*** sunt sedimentate pe versanții domoli care se mărginesc cu terasele, sunt prezentate prin argile nisipoase compacte mai rar cu compacticitate medie, grosimea stratului - 0,5-5m.

* ***depunerile de alunecări contemporane ale cuaternarului superior*** sunt răspîndite pe malul drept al văii râul Delia și pe malurile surpate ale râul Prut. Sunt prezentate prin grund cu structură și stratificare neregulată ale rocilor de bază. În partea superioară a versanților cu alunecări aceste grunduri se caracterizează prin textură primară neregulată. Depunerile de alunecări din partea inferioară a versantului se caracterizează prin deteriorare totală a texturii și particularitățile structurale (argile, argile nisipoase, nisipuri). grosimea stratului variază de l a1-2m pînă la 10-15 m.

#### Depozitele contemporane ####
Sunt prezentate depunerile aluviale din lunca înaltă și joasă, depunerile aluviale-proluviale, depunerile lacurilor artificiale, solul fertil și sol tehnogen.

* ***depunerile aluviale*** - sunt sedimentate în lunca înaltă și cea joasă a râul Prut și Delia. Sunt constituite din alternanță de straturi de argile nisipoase, sol nisipo-lutos, argile, grosimea stratului - pînă la 15,0 m.

* ***depunerile aluviale-proluviale*** sunt sedimentate pe fundul vîlcelelor. Sunt constituite din argile nisipoase, argile cu alternanța stratului de argile nisipoase și nisipuri. grosimea stratului pînă la 3-4 m.

* ***depunerile lacurilor artificiale*** - pînă la gura de vărsare a rîului în lac, sunt constituite din argile nisipoase compacte și nămol, grosimea stratului pînă la 3-4 m.

  - ***stratul de sol fertil*** - are răspîndire limitată, - doar în locurile fără construcții, grosimea stratului 0,3 - 0,5 m, mai rar 0,7 m. Conținutul de humus în sol variază de la 2,0 pînă la 3,6 %.
  - ***sol tehnogen*** - diverse rămășițe ale solului natural, deșeurile de producere, deșeuri menajere. tipul sedimentării - straturi, terasament, umplutură de pămînt, grosimea stratului pînă la 2-3 m.

## Condițiile hidroheologice ##
În limitele teritoriului orașului se evidențiază următoarele orizonturi și complexe acvifere:

* ***orizontul acvifer al depunerilor aluviale-deluviale contemporane ale cuaternarului superior*** care coincide cu valea râul Prut și Delia. În lunca râul Prut orizontul acvifer se află la adîncimea 1,6 - 6,5 m, capacitatea 4-5,7 m. Sunt ape cu nivel liber, sezoniere, apele subterane variază - 1,5 m. Orizontul acvifer este alimentat din contul precipitațiilor atmosferice, iar în perioada viiturilor - debitul de suprafață. Direcția deplasării apelor subterane coincide cu direcția albiei. Viteza infiltrării în apropierea r-nului Berești - pînă la 0,55 m / zi, în apropierea Ungheniului Inferior pînă la 1,5 m / zi.17 Apele orizontului sunt slab mineralizate și sunt utilizate în calitate de apăpotabilă.

* ***orizontul acvifer al depunerilor aluviale-deluviale*** - sunt răspîndite în argilele nisipoase. Grosimea stratului 1-2 m, gradul de infiltrare a apei este joasă, cu nivel liber. Orizontul este alimentat din contul infiltrării apelor drenate ale orizontului acvifer. Apele nu sunt protejate împotriva poluării cu deșeurile menajere, apele reziduale de la întreprinderi, îngrășăminte, se caracterizează prin  gresivitate sulfatică la beton.

* ***orizontul acvifer din depunerile de alunecări*** - adîncimea sedimentării 1-4,5m, sunt alimentate din contul infiltrării precipitațiilor atmosferice și scurgerile de pe terasele din amonte și depunerile sarmațianului mediu. Descărcarea orizontului are loc prin scurgerea de pe pantă sau izvoare spre fundul vîlcelelor și în valea rîurilor. Compoziția chimică hidrocarbonat, calcică, magnezică, gradul de infiltrare scăzut.

## Condițiile hidrologice ##
Rîul Prut se alimentează din apele subterane, apele provenite din topirea zăpezii și precipitațiile atmosferice. Odată cu punerea în folosință a nodului hidrotehnic Costești-Stînca, care are menirea de a regulariza apele mari, debirul maxim al râul Prut în secțiunea or. Ungheni a scăzut. Debitul apei corespunde nivelului 37,5 m. După trecerea viiturii are loc scăderea nivelului apei care durează pînă în luna august-septembrie.

În sezonul de iarnă nivelul rîului este mai puțin stabil, variația depinde de frecvența moinelor, care sunt însoțite de ploi. Pentru perioada formării podului de gheață este caracteristică

Regimul hidrologic al râul Prut este afectat de alunecările pe sectoarele unde rîul este limitat de lunca înaltă și are loc eroziunea treptelor teraselor.

Construcția lacului de acumulare a sporit nivelul apelor subterane, a contribuit la reținerea apelor orizonturile acvifere în solul care aderă la lac. Schimbarea stăvilii naturale al nivelului poate contribui la activizarea proceselor de alunecări pe versantul de sînga a văii râul Delia.

## Zonificarea geotehnică a teritoriului ##
După gradul favorabilității pentru construcții și nivelul afectării de alunecări pe
teritoriul orașului se evidențiază 3 zone:

* 1 - zona favorabilă, fără alunecări (I și II terase de-asupra luncii);
  * a) subzona răspîndirii rocilor tasabile (tipul I și II de tasabilitate). Construcțiile noi pot fi amplasate numai după efectuarea investigațiilor detaliate în scopul stabilirii  articularităților fizico-mecanice a grundului pentru fiecare obiect separat.
  
  * b) subzona versanților - sectoarele înguste neafectate de alunecări ale versanților teraselor înalte a râul Prut, versantul de dreapta a văii râul Delia, malul de sud- est al lac. delia, versanții de stînga a afluenților râul Prut și Delia.    Înclinarea pantei versanților - r-nul Berești - 3-5 0 ; malul de sud-est a lac. Berești - 17-25 0 . Versanții se caracterizează ca fiind stabili. La amplasarea construcțiilor noi este necesară  fectuarea investigațiilor geotehnice detaliate în fundamentării terasării și scurtarea versanților.
 
  * c) subzona nivelului ridicat al apelor subterane - teritoriile neafectate de viituri în limitele I terase de-asupra luncii. Apele subterane se află la adîncimea pînă la 4,5 m. În partea superioară al secțiunii geologice argilele au proprietăți de umflare. La amplasarea construcțiilor noi este necesar de ținut cont de nivelul apelor subterane, posibilitatea inundării construcțiilor cu subsol.

  * d) subzona teritoriilor inundate - 3 sectoare pe râul Prut în limitele orașului cu nivelul asigurării 1 %. Influența viiturilor este regularizată de lacul de acumulare Costești - Sînca. 
  Regularizarea sezonieră a debitului râul Delia este asigurată de baraj cu nivelul de reținere a apei - 40,8 m. Construcțiile noi pot fi amplasate numai după efectuarea măsurilor de prevenire a posibilelor inundații.

  * e) subzona proceselor de înmlăștinire și înnămolire - zona de sud-est a lac. Delia, valea rîurilorâul Sunt necesare măsuri de regularizare a debitului rîurilor.

* 2 - zona nefavorabilă cu alunecări potențiale -
  * 1 subzonă - pe versantul de stînga a văii râul Delia cu cotele de la 40,8 m pînă la 61,0 m se caracterizează prin alunecări active de tip frontal. Zona de parc - plantațiile necesită protecție și întreținere.
  
  * 2 subzonă - 3 sectoare separate afectate de alunecări pe malul râul Prut. Ținînd cont de mecanismul dezvoltării alunecărilor, pentru stabilizarea versanților sunt necesare un șir de măsuri împotriva eroziunii, proceselor de surpare - prăbușire, desecarea și reținerea mecanică a alunecării maselor de grund.

* 3 - zona condiționat favorabilă cu alunecări potențiale - manifestarea nemijlocită a proceselor de alunecări lipsește, însă persistă tendința activizării proceselor de alunecare. Construcțiile noi pot fi amplasate numai după efectuarea investigațiilor geotehnice detaliate și calculelor ce confirmă stabilitatea versanților.

Concluzii:
* 1. După gradul afectării de alunecări teritoriul este divizat în 3 zone, neafectată de alunecări, afectată de alunecări și cu alunecări potențiale.

* 2. Cea mai mare parte a orașului după gradul afectării de alunecări se referă la cea fără alunecări. Segmentele cu procese de alunecări active ocupă o parte neînsemnată a teritoriului și sunt răspîndite predominant peste zona construită.

* 3. După gradul favorabilității pentru construcții teritoriul orașului se caracterizează ca fiind condiționat favorabilă.

* 4. În structura geomorfologică a teritoriului participă depozitele contemporane ale cuaternarului superior, care sunt sedimentate pe terase și versanții văii râul Prut, depunerile aluviale ale cuaternarului mijlociu, neogen-cuaternare, neogene.

* 5. În hotarele teritoriului examinat sunt evidențiate 2 complexe acvifere: apele subterane ale luncii râul Prut și afluenții lui; apele subterane cuaternare ale teraselor râul Prut. Apele acestor orizonturi după compoziția chimică se caracterizează prin agresivitate sulfatică la beton. Ridicarea nivelului apelor subterane ținînd cont de variația sezonieră poate atinge cote de 1,5 m, iar în unele cazuri - pînă la debușarea lor la suprafața terestră.

* 6. Seismicitatea teritoriului orașului Ungheni, conform Hărții raionării seismice a RM, - 7 grade.

* 7. Partea de luncă a teritoriului râul Prut, unde lipsește barajul și digul este inundată de apele viiturilor.

* 8. Cota, nivelului de reținere normală a nivelului iazului Delia - 40,8 m.



< Înapoi la [Cuprins](../cuprins.md)