< Înapoi la [Cuprins](../../cuprins.md) | [10. Căi de comunicații și transport](../v1_cap10.md)
# 10.2 Reglementări #

Prognoza dezvoltării sistemului de transport.
Măsuri pentru dezvoltarea rețelei stradale
Propunerile de proiect pentru dezvoltarea reţelelor stradale în or. Ungheni
reprezintă etapa reformatoare a schemei curente şi se bazează pe deciziile principiale
ale structurii de transport orăşeneşti, ţinînd cont de prioritatea construcţiilor
drumurilor magistrale, perspectiva dezvoltării transportului public şi alte decizii de
proiect, care determină tipurile şi volumul lucrărilor de construcţie şi reconstrucţie.
Este prezentată structura rețelei de drumuri pentru perioada de calcul cu propuneri înţ
funcţie de destinaţie şi clasificare a magistralelor.
La formarea de perspectivă a reţelei stradale, ţinînd cont de condiţiile oraşului,
ca obiectiv s-a stabilit mărirea conexiunilor de bandă între zone ale orașului.
La baza deciziilor de proiectare a reţelelor stradale stau următoarele condiții:
implementarea legăturilor importante de transport între sectoarele oraşului prin
cele mai scurte direcţii
legaturi auto şi pietonale comode între zonele locative şi cele industriale
ieşire la automagistrale externe.
Proiectul prevede diferenţierea drumurilor existente şi cele proiectate,salvate
pentru perspectivă, după următoarea clasificare:
strazi de importanţă orăşenească
strazi de importanţă regională
străzi locale
Elementele de construcţie a noilor străzi sînt adoptate în funcţie de laţimile
tipice în conformitate cu СНиП 2.07.01-89* «Urbanism».
Schema reţelei stradale propusa de proiect oferă o structură clară a
magistralelor şi strazilor locale. Cu toate acestea, pe drumurile magistrale trebuie
prevăzute buzunare de oprire pentru transportul public, de asigurat marcaje
corespunzătoare pentru partea carosabilă şi organizarea curculaţiei rutiere.
Schema rețelei de drumuri va permite rezolvarea problemelor de trafic, în
măsura etapelor de implementare.

MO- magistrală de interes orășenesc
MR- magistrală de interes raional
* - străzile existente după reconstrucţie îşi păstrează lațimea părţii carosabile
în caz că este mai lată decît cea propusă în planul general.În total vor fi reabilitate şi construite drumuri de interes orășenesc și raional
cu o lungime de 52,125 km, pentru perioada estimată, inclusiv 27,58 km vor constitui
construcțiile noi.
De asemenea, vor fi reparate
şi construite noi străzi de interes local – 38,8 km, inclusiv 11,92km drumuri noi

La baza proiectării dezvoltării rețelelor stradale stau următoarele condiții:

Se efectuează o clasificare clară a străzilor după ;

Străzile de interes orășenesc se lărgesc între liniile roşii pînă la 40m, iar cele de
interes raional pînă la 40 m. În dependenţă de categoria străzii, se extinde partea
carosabilă. Cartierele se extend, iar străzile blocate se transformă în drumuri de acces
intermediare. Pentru rezolvarea schemei generale a reţelei stadale au fost folosite la
maxim străzile pavate;

Rețeaua planificată de străzi legată funcțional de centrul orașului, zonele
rezidențiale, zone industriale şi o zonă de relaxare.

Proiectul oferă posibilitatea de a înfăptui legăturile de
bază între raioane.
Un rol deosebit în funcţionarea normală a rețelei de drumuri în condiţiile
reliefului dificil îl dețin instalaţiile complexe a transportului rutier.
Proiectul propune construcţia a două noduri rutiere în 2 nivele și două intersecții
într-un nivel.
În 2 nivele:

str. Plămădeală ( ieşire spre Chișinău) - drum de ocolire;

str.Petru Rareş — drum de ocolire.
În 1 nivel:

str. Ştefan cel Mare— drum de ocolire;

ieşire la sud, spre Nisporeni— drum de ocolire.
Construcția acestor noduri rutiere va permite dirijarea mai efectivă a
circulaţiei împreună cu alte măsuri de gestionare a traficului.
Ciclismul şi legăturile pietonale
Piste pentru biciclete sunt aranjate în conformitate cu reglementările şi secţiunile
transversale de însoţire. Străzile pietonale sunt prevăzute în parc şi în zonele de
recreare.
Transportul public
În legătură cu construcţia noilor străzi, se propune încă 3 rute pentru
transportul public cu lungimea totală de 28,91 km. Rutele propuse:

str. Ştefan cel Mare 275, str. Z. Arbure, str. Burebista, str. P. Rareş, tr-la
Coăureni, str. M. Cogălniceanu, str. Petru Movilă, str. Decebal, str. Putna .

str. Ştefan cel Mare 301– str. Danuţeni – str-la Şleahului – str. Ştefan cel Mare
– str. Romană – str.Ion Creangă – str. G. Cristiuc – f-ca „Biovit”

str. Deleşti – str-la Găureni– str. Ştefan cel Mare – str. Alexandru cel Bun –
strada nr.7
Transportul extern
Pentru staţia auto din or. Ungheni, în perioada de calcul, nu se prevede o
creştere considerabilă a transportului de pasageri, acesta fiind stabil. Se propune
modernizarea platformelor de sosire, plecare şi staţionare a autobuzelor şi
microbuzelor, de asemenea , amenajarea întregului teritoriu al gării.105
Transportul feroviar
În temeiul Strategiei de transport şi logistică pentru anii 2013 – 2022, aprobate
prin Hotărîrea Guvernului nr. 827 din 28.10.2013este preconizată elaborarea studiului
de fezabilitate a terminalului pentru transportarea mărfurilor la staţia Ungheni în anul
2014 şi funcţie de rezultatele studiului de fezabilitate va fi decisă executarea
construcţiei pînă în anul 2018.
Volumul maxim de transportare a pasagerilor se înregistrează în perioada de
vară. Coeficientul fluctuaţiilor sezoniere este de 1,3.
Garaje şi locuri de parcare.
Staţionarea şi parcarea transportului individual se preconizează sa fie organizat
în felul următor:
- autoturismele proprietarilor care locuiesc în sectorul privat – în zona de reşedinţă a
acestora;
- în clădirile multietajate noi construite trebuie sa fie asigurate locuri de parcare
permanente a automobilelor sau construcţia garajelor sau parcărilor la o distanţă
normată de la locul de trai.
Problema parcărilor temporare se propune de a fi rezolvată prin orientarea în 2
direcţii, şi anume:
- parcările temporare, pe lîngă cele existente, se propun a fi organizate din contul
reţelei de drumuri a raionului și între spațiul dintre magistrale. Parcările trebuie să fie
organizate, păzite şi contra plată. Pe automagistrale se pot păstra doar parcările ce nu
ocupă benzile principale, cu asigurarea intrărilor şi ieşirilor fără a crea dificultăţi
traficului.

Parcările 24/24 urmează a fi amplasate pe perimetrul sectorului central, de
asemenea în zona subterană a clădirilor de locuit şi a edificiilor publice noi.
Pentru perioada de proiectare a or. Ungheni este necesar următorul număr de
garaje:
- în sectorul privat— în curtea clădirilor de locuit 600 unităţi;
 în sectorul construcțiilor multietajate, luînd în consideraţie noile microraioane
este necesar :



< Înapoi la [Cuprins](../../cuprins.md) | [10. Căi de comunicații și transport](../v1_cap10.md)