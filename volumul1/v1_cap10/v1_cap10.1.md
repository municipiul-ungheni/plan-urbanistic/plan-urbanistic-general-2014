< Înapoi la [Cuprins](../../cuprins.md) | [10. Căi de comunicații și transport](../v1_cap10.md)
# 10.1 Situaţia existentă #
## Reţeaua stradală ##
Oraşul Ungheni este un oraş transfrontalier situat în zona centrală a Republicii
Moldova fiind traversat de două drumuri de importanţă naţională R1 (Chişinău –
Ungheni – Sculeni – fr. cu România) şi R42 (Ungheni – Măcăreşti – Bărboieni), prin
intermediul cărora stabileşte legături de transport cu toate oraşele, comunele şi satele
republicii, precum şi ţările învecinate Ucraina şi România. Oraşul se întinde pe o
distanţă de 9 km de-a lungul frontierei cu România. Laţimea medie a oraşului este
aproximativ de 3 km.
În or. Ungheni sunt 171 străzi cu o lungime totală de 132,4 km şi o suprafaţa de
cca. 0,87 km 2 . Densitatea liniară a străzilor în raport cu teritoriului valorificat
constituie 7,9km/km
Schema reţelei de drumuri şi străzi a oraşului s-a format reieşind din relieful
natural şi construcţiile existente, care reprezintă prin sine un sistem dreptunghiular, în
deosebi în partea central a oraşului, divizat în cartiere cu dimensiunile 200 x 150 m-
200 m. În partea veche a oraşului structura reţelei de străzi este haotică.
Ţinînd cont de sporirea nivelului de motorizare din ultimii ani, schimbările în
structura transportului public, numărul călătoriilor de afaceri, şi în special, cele ce ţin
de activitatea comercială a populaţiei, reţeaua de drumuri existentă cu capacitatea de
trecere şi parametrii tehnici minimi nu corespunde cerinţelor contemporane.
Situaţia se agravează şi din cauza lipsei posibilităţilor alternative a legăturilor de
transport, ceea ce contribuie la formarea în centrul oraşului a fluxului de transport
interrurban în tranzit, care constituie aproximativ 50%.
De asemenea, centrul oraşului îndeplineşte funcţia de nod de transbordare în
reţeaua transportului public urban, fiind unul din principale punctele de sosire şi
plecare a pasagerilor.
Reieşind din cele expuse se poate constata că, magistralele din centrul oraşului,
care sunt prevăzute pentru stabilirea legăturilor cu sectoarele de la periferia oraşului,
nu corespund funcţiilor sale şi necesită reconstrucţie cu aducerea parametrilor tehnici
în corespundere cu normativele în vigoare.
Ca urmare, străzile Naţională, Romană şi Decebal sunt străzi principale ale
oraşului, aici sunt concentrate multe obiecte cu număr mare de vizite care generează
fluxurile mari a transportului şi de pasageri.
Pentru soluţionarea acestei probleme, în primul rînd este necesară diferenţierea
precisă a străzilor conform destinaţiei funcţionale a lor cu nominalizarea principalelor
magistrale care necesită reconstrucţie.
Străzile principale ale oraşului sunt Naţională, Romană, Decebal, Alexandru cel
Bun, Ştefan cel Mare, care îndeplinesc funcţia de magistrale de importanţă urbană,
dispun de îmbrăcăminte rutieră rigidă şi se află în stare tehnică relativ satisfăcătoare
Dezavantajul constă în nenormarea lăţimii părţii carosabile şi a liniilor roşii care
sunt strâmtorate de construcţiile existente. Legătura între centrul oraşului şi celelalte
sectoare se stabileşte prin intermediul străzilor, multe dintre care se află un stare
tehnică nesatisfăcătoare, sau nici nu dispun de îmbrăcăminte rutieră. Ca urmare toată
încărcătura rutieră revine sectorului centru al oraşului.93
Parametrii străzilor
Străzile principale ale oraşului sunt Naţională, Romană, Decebal, Alexandru cel
Bun, Ştefan cel Mare care dispun de trotuare şi parte carosabilă de la 7,0 până la 15
m. Unele segmente ale acestor străzi necesită reparaţie şi reconstrucţie.

Pentru determinarea corespunderii fluxului de transport pe teritoriul oraşului,
stabilirea nivelului de încărcătură a reţelei de drumuri şi străzi magistrale, la data
12.03.2014 au fost efectuate investigaţii ale transportului. Aceste investigaţii
includeau: măsurarea intensităţii circulaţiei cu îndeplinirea fişelor corespunzătoare
conform tipului transportului pe 14 segmente a străzilor principale ale oraşului.
Conformarea fluxului la automobilul convenţional a fost efectuată cu utilizarea
coeficienţilor corespunzători stipulaţi în СНиП 2.05.02-85 (p.1.3). Cartograma
intensităţii circulaţiei transportului obţinută în rezultatul calculelor efectuate este
reflectată pe schemă. Ca urmare au fost evidenţiate segmentele străzilor din oraş cu
intensitatea sporită. În tab. 2. Anexă, sunt reflectate datele obţinute în rezultatul
investigaţiilor în "orele de vârf".94
În rezultatul evaluării structurii deja existente a reţelei de drumuri şi străzi se
poate constata:
- Străzile cele mai aglomerate sunt – străzile Decebal, Ştefan cel Mare,
Naţională şi Romană. Pentru a reduce fluxul de pe aceste străzi este necesară
implementarea propunerilor privitor la organizarea circulaţiei.
- Pentru perioada actuală este oportună modificarea structurii fluxului de
transport în deosebi ceea ce ţine de sporirea numărului autoturismelor şi reducerea
autocamioanelor care este condiţionată de situaţia socio-economice creată.
La momentul investigaţiei transportul mărfar constituie 12-28 % din numărul
total al unităţilor de transport, cota autoturismelor constituie 72-88%.
- Lungimea reţelei străzilor magistrale nu corespunde normativelor în vigoare,
deoarece prevederile stipulate în Planul urbanistic general precedent, pentru un şir de
străzi şi drumuri cu rol important în Schema transportului, nu au fost implementate.
- Numărul insuficient de parcări amenajate "platforme de staţionare" pentru
staţionarea transportului public, nivelul scăzut de organizare a circulaţiei, calitatea
redusă a îmbrăcămintei rutiere contribuie la capacitatea, şi aşa redusă, a reţelei de
drumuri şi străzi, formarea ambuteiajelor, poluarea aerului cu gaze de eşapament,
inclusiv sporirea numărului de accidente rutiere.
- Străzile de importanţă locală din centrul oraşului, pe toată lungimea sa, cu
capacitatea parametrilor tehnici scăzuţi sunt tranzitate de fluxul semnificativ al
transportului.
- Toate străzile, inclusiv de importanţă locală, cu excepţia unor segmente sunt
utilizate pentru parcări provizorii şi de lungă durată, neamenajate, ceea ce contribuie
şi mai mult la reducerea capacităţii de trecere.
## Organizarea circulaţiei ##
În situaţia creată, când reţeaua de drumuri şi străzi trebuie să corespundă
normativelor ce ţin de capacitatea de trecere a transportului şi a pietonilor, cât şi
reconstrucţia edificiilor, o deosebită atenţie trebuie acordată organizării circulaţiei.
Reieşind din parametrii tehnici scăzuţi al reţelei de drumuri şi străzi actualmente
în oraş se practică organizarea circulaţiei după principiul circulaţiei cu sens unic pe
străzile paralele. În sectorul centru al oraşului acest tip de circulaţiei se practică pe str.
Naţională şi Romană. Introducerea circulaţiei cu sens unic pe străzile cu funcţii de
magistrale are un şir de priorităţi, principalele fiind:
- evitarea cazurilor de circulaţie contrasens a unităţilor de transport şi excluderea
pericolului de accidentare;
- utilizarea raţională a benzilor părţii carosabile;
- imbunătăţirea condiţiilor de coordonare a dirijării circulaţiei cu ajutorul
semafoarelor.
În lista dezavantajelor rezultate din stabilirea circulaţiei cu sens unic trebuie
inclusă şi utilizarea conform acestei Scheme a transportului public de pasageri, din
cauza extinderii accesului pietonilor şi a distanţei parcurse. Concomitent, aceste
dezavantaje sunt minimale reieşind din distanţa mică dintre străzile cu sens unic care
nu depăşeşte 200m, ceea ce este caracteristic pentru sectorul centru a oraşului.
Actualmente, principalul element în dirijarea circulaţiei transportului în oraş
serveşte dirijarea intersecţiilor cu ajutorul semaforului care are ca scop utilizarea95
eficiente a capacităţii de trecere a reţelei de drumuri şi străzi. Ceea ce contribuie într-
o oarecare măsură la reglementarea circulaţiei şi regularizarea vitezei fluxului de
transport.
Ca urmare, perfecţionarea continue a sistemului de dirijare a circulaţiei cu
ajutorul semaforului şi implementarea tehnologiilor moderne, vor contribui,
suplimentar, la sistematizarea circulaţiei transportului în oraş.

Sporul numărului unităţilor de transport înregistrate în mediu constituie 3,77 %
în an. Spre regret, numărul accidentelor rutiere creşte proporţional sporului nivelului
de motorizare. Nivelul de motorizare în oraş constituie 52 automobile la 1000
locuitori.

## Transport public ##
Transportul public urban al or. Ungheni este 2 tipuri – maxi-taxi şi taxi.
Serviciile de transport sunt prestate de 3 companii ce oferă transport cu microbuze.
Lungimea reţelei de transport public pe rutele fixe constituie 90,2 km.
În partea centrală este cel mai mare trafic de pasageri fiind condiţionat de piaţa
agricolă, gara auto, obiective ale infrastructurii sociale.
Oraşul este deservit de 6 rute de microbuze, acesta fiind cel mai solicitat tip de
transport deoarece dezvoltă viteza relativ mare şi intervalul de timp între ture relativ
mic. Microbuzele deservesc 6 rute , cu un număr zilnic de 24 unităţi. Lungimea
traseului de maxi-taxi constituie 90,2 km, lungimea rutelor variază de la 13 pînă la 18
km. De-a lungul traseelor nu sunt delimitate şi amplasate staţii a transportului urban.
Conducători microbuzelor încarcă şi descarcă pasagerii la solicitare. Aproape toate
rutele intersectează zona de centru a oraşului. Pentru utilizarea serviciului se percepe
un tarif de 3 lei per călătorie. Acest tip de transport este foarte solicitat în oraş datorită
vitezei şi intervalului relativ mic între rute.
Toate rutele îşi au începutul şi traversează partea centrală a oraşului, astfel
stabilind legături cu celelalte sectoare ale oraşului, ca rezultat se formează ambuteiaje
şi se crează condiţii pentru accidente de circulaţie şi poluarea mediului.
Actualmente, practic pe toate rutele în "orele de vârf" se înregistrează
insuficienţa de transport, din care cauză microbuzele sunt supraaglomerate, astfel
creând disconfort pasagerilor şi sporind situaţiile de traumatism. Pentru soluţionarea
problemei respective este necesară revizuirea graficului de circulaţie, în aşa mod, ca
în "orele de vîrf" să circule autobuze sau sporirea numărului de microbuze pe rute.

## Garaje şi parcări ##
Actualmente, pe teritoriul or. Ungheni aproximativ 300 unităţi de transport sunt
păstrate în garaje staţionare, iar celelalte – în sectorul privat.
În oraş sunt înregistrate oficial 2 autoparcări, cu o suprafaţa totală de 900 m 2
pentru 150 maşini-locuri. Costul pentru staţionare este de 5 lei/zi. Numărul existent
de parcari în oraş este suficient
Trebuie de menţionat faptul că, prezenţa obiectivelor publice şi comerciale în
centrul oraşului atrag mult transport, şi astfel, se crează deficitul de parcări în timpul
zilei practic pe toate străzile, inclusiv şi cele magistrale sunt folosite ca parcări
provizorii. Pentru parcare sunt folosite spaţiile de pe trotuare, în deosebi în apropierea
pieţei, magazinelor şi altor obiective prestări servicii.
Ca urmare, numărul insuficient de locuri pentru parcare reduce capacitatea de
trecere a străzilor, circulaţia transportului devine dificilă, sunt create condiţii pentru
accidentele rutiere, şi ca urmare crează incomodităţi pentru transport şi pietoni.
Conform Planului urbanistic general pentru perioada de calcul, este oportun de a
repartiza locuri pentru păstrarea de lungă şi scurtă durată a transportului auto.

## Transport periferic ##
Transportul periferic în or. Ungheni este reprezentat prin 3 tipuri – feroviar, auto
şi fluvial, care asigură legăturile de transport şi economice, leagă oraşul cu alte
localităţi ale Republicii Moldova cît şi cu ţările Europei de Vest.
Gara auto asigură următoarele legăturile de transport - 7 în tranzit, 10
internaţionale, 18 interurbane şi 38 interrurale. Informaţia referitor la numărul
pasagerilor transportaţi

Conform datelor reflectate în tabel, se poate constat că, în ultimii ani s-a
înregistrat scăderea traficului de pasageri cu 6,8 %.
Staţia de cale ferată Ungheni este punct internaţional de trecere unde se
efectuează traficul de mărfuri şi de pasageri.

La nodul de cale ferată Ungheni nu sunt subdiviziuni ale Î.S. „Calea Ferată din
Moldova„ care execută lucrări de reparaţie capitală a materialului rulant. Depoul de
întoarcere a locomotivelor şi staţia de revizie a vagoanelor, amplasate la staţia
Ungheni, efectuează deservirea şi reparaţia curentă a materialului rulant.
Oraşul dispune şi de un port fluvial pe r. Prut care se află în gestiunea
întreprinderii de Stat „Portul Fluvial Ungheni”. Portul Fluvial Ungheni este unul din
cele patru porturi fluviale existente în prezent în Republica Moldova. În ultimii ani a
continuat amenajarea r. Prut. Recent, în acest sens pentru întreţinerea sectorului
tehnic Ungheni, au fost efectuate lucrări de adîncime şi îndreptare a albei r. Prut pe
un sector cu lungimea de 385-405 km.


< Înapoi la [Cuprins](../../cuprins.md) | [10. Căi de comunicații și transport](../v1_cap10.md)