< Înapoi la [Cuprins](../cuprins.md)

# 6. Dezvoltarea infrastructurii sociale #
* [6.1. Instituții de educație](v1_cap6/v1_cap6.1.md)
* [6.2. Obiective de ocrotirea sănătății](v1_cap6/v1_cap6.2.md)
* [6.3. Obiective cultură, sport și agrement](v1_cap6/v1_cap6.3.md)
* [6.4. Obiective comerț, alimentație publică și deservire comunală](v1_cap6/v1_cap6.4.md)

Reţeaua obiectelor de deservire socială include instituţiile de educaţie, medicale,
cultură, construcţii sportive, centre comerciale şi alimentare publică.
Infrastructura socială are ca scop crearea condiţiilor optime pentru abitaţie şi
satisfacerea necesităţii în obiectele de educaţie preşcolară, învăţămînt, ocrotire a
sănătăţii, comerţ şi alimentaţie publică, cultură, sport şi agrement.
Actualmente, obiectele prestări servicii sunt amplasate neuniform pe teritoriul
oraşului, iar numărul şi calitatea serviciilor prestate nu corespund exigenţelor
contemporane. Multe obiecte sunt amplasate în încăperi adaptate fără dotare edilitară
necesară.
Însă în condiţiile moderne au apărut şi un şir de factori pozitivi care influenţează
dezvoltarea reţelei obiectelor prestări servicii. Acestea sunt: apariţia diverselor forme
de proprietate care stimulează calitatea şi diversitatea tipurilor de servicii, construcţia
obiectivelor după proiecte individuale ce permite crearea unei individualităţi
arhitecturale şi estetice a localităţii.
Calculul capacităţilor reglementate pentru obiectele prestări servicii este efectuat
ţinînd cont de numărul calculat al populaţiei, analiza indicilor specifici obţinuţi la
1000 locuitori şi majorarea lor pînă la normele minim necesare pentru perioada
proiectată, repartizarea uniformă a obiectelor de deservire în formaţiunile locative şi
asigurarea populaţiei cu servicii cotidiene necesare.
Deoarece, oraşul Ungheni este subcentru al sistemului central de populare pe
grupe capacitatea obiectivelor de importanţă orăşenească şi extraurbană (obiectivele
de ocrotire a sănătăţii, cultură şi agrement, centre comerciale, complexe sportive,
hoteluri) se stabileşte ţinînd cont de deservirea interrurală şi localităţile din raza de
deservire.

< Înapoi la [Cuprins](../cuprins.md)