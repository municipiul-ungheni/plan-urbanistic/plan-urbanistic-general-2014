< Înapoi la [Cuprins](../../cuprins.md) | [3. Potențialul economic](../v1_cap3.md)

# 3.1. Caracterizarea situaţiei existente #

Oraşul Ungheni este situat pe malul estic al r. Prut, vis-a-vis de satul omonim
din România cu care a alcătuit în trecut aceeaşi localitate. Poziţia geografică specială,
la răscruce de drumuri, a constituit cheia pentru ascensiunea Ungheniului la statutul
de urbă. Datorită unei astfel de aşezări, s-a decis în anii '70 ai sec. XIX construcţia
căii ferate Chişinău -Iaşi, care să treaca prin Ungheni.
Oraşul Ungheni este unul din cele mai active oraşe din Republica Moldova, în
ce priveşte atragerea resurselor financiare din proiecte, alocarea granturilor şi
investiţiilor pentru dezvoltarea economiei şi infrastructurii oraşului. În oraş au fost
create zone economice libere pentru activitatea de antreprenoriat, întreprinderi mixte
cu participarea investişiilor străine. Avantajul plasării geografice constă în căile de
tranzit şi vamale de ieşire spre Europa Centrala, condiţii pentru dezvoltarea unei zone
agricole ecologic pure.
Economia oraşului este condiţionată de un şir de factori printre care se
evidenţiază:
iversitatea bazei de materii prime amplasate pe teritoriile adiacente care
contribuie la dezvoltarea ramurilor prelucrătoare a producţiei agricole;
reţeaua de drumuri de importanţă naţională şi locală bine dezvoltată, fiind un
important nod feroviar şi punct de frontieră vamal ce conecteaza reţeaua internă
cu exteriorul;
infrastructura socială şi de producere dezvoltată - învăţământ, cultură, sport,
domeniul prestări servicii, dotarea tehnico - edilitară, etc.
Actualmente baza urbană a oraşului este prezentată de întreprinderi din diverse
ramuri ale economiei - industria alimentară, uşoară, de confecţii şi a materialelor de
construcţii. Lista principalelor întreprinderi din oraş este reflectată în tab. 3.1.1.

Industria oraşului este caracterizată prin producţia de covoare, ceramică, vinuri
şi divinuri, conserve, confecţii, articole de beton armat şi lemn, mezeluri, de
panificaţie, comerţul şi serviciile se înscriu în principalele domenii de activitate
economică.
În industria alimentară principalele întreprinderi sunt:
 „Ungheni Vin” - producerea şi depozitarea vinului;
 SRL .ÎCS „Ecovit”- producerea conservelor şi sucurilor;
 SA „Cereale-Prut” depozitarea produselor agricole;
 SRL „Danova fabrica de pîine” fabricarea produselor de panificaţie;
 SRL "Plum-Com" - prelucrarea fructelor etc.
Industria uşoară este una din subramurile cele mai reprezentative ale oraşului. În
1980 a fost fondată întreprinderea „Covoare-Ungheni” care este cea mai mare fabrică
de covoare din Moldova cunoscută în ţară şi de peste hotare pentru calitate şi
originalitatea lor. În 2001, fabrica „Covoare Ungheni” a fost divizată în două
întreprinderi separate: „Moldabela”, care produce covoare şi „Covoare Ungheni”,
care produce fibre textile - materie primă pentru covoare.
În oraş funcţionează compania americană SRL ÎCS "Lear Corpration" care
confecţionează huse şi alte articole textile pentru autovehicole, numărul scriptic al
angajaţilor 1800 oameni.
Printre întreprinderile din industria materialelor de construcţii sunt SRL "Cons-
Viand", SRL "Sampdoria", „Javelin N.A” S.R.L etc.
În domeniul ceramicii: „Ceramica-Ungheni” S.A., unica întreprindere de
ceramică din republică.
Concomitent, în oraş funcţionează un şir de întreprinderi din industria mobilei şi
prelucrarea lemnului - ÎM "Euro Yarns", SRL "Scopos", SA "Comoditate".
ZEL „Ungheni-Business”
În anul 2002 în partea de nord-vest a oraşului în zona industrială a fost înfiinţată
Zona Economică Liberă “Ungheni-Business” pe o suprafaţă de 42,34 ha. Amplasarea
favorabilă a ZEL, cu acces la calea ferată favorizează stabilirea legăturilor cu
regiunea de nord, centru şi sudul Republicii, precum şi cu ţările din exterior. Zona
este creata pe baza a 13 întreprinderi de diferite ramuri, zona dispune de toata
infrastructura necesara pentru dezvoltarea afacerii (acces la drumuri auto locale si
internationale, acces la calea ferată de tip european şi rusesc, apeducte, canalizare,
sisteme de telecomunicaţii şi termificare, gazificare, energie electrică, depozite).
La 1 ianuarie 2011, pe teritoriul zonei libere erau înregistraţi 34 de rezidenţi,
dintre care întreprinderi cu capital din Italia, Austria, România, Rusia şi Belgia.
Numărul angajaţilor constituie peste 1800 de persoane. Unul dintre cei mai mari
rezidenţi în ZEL „Ungheni-Business” este corporaţia americană „Lear” care produce
huse şi alte articole din textile pentru industria de automobile.
Dezvoltarea cu succes a ZEL "Ungheni-Business" se datorează mai ales
proximităţii graniţei cu Uniunea Europeană, în care după aderarea României la UE s-
au stabilit mai multe companii din România, UE şi Turcia, în căutare de facilităţi şi
forţă de muncă mai ieftină.
Un grup semnificativ de organizaţii care participă la activitatea economică a
oraşului sunt obiectivele de depozitare, comerciale şi de achiziţionare. Multe din
aceste obiective îndeplinesc funcţii de intermediari în asigurarea cu materie primă şi
comercializarea producţiei finite. Principalele întreprinderi de depozitare pe teritoriul
oraşului sunt - SRL "Danova-Prim", SA "Cereale-Prut", SRL "Electro", SRL SC
"Unconsalex".
În ultimii ani a sporit numărul angajaţilor în sfera prestări servicii. Care este
reprezentată în deosebi prin firme mici, care prestează populaţiei servicii în
construcţii, reparaţii, reparaţia electrocasnice, confecţionarea îmbrăcămintei, servicii
comerciale, de transport etc. Acest tip de serviciu este reprezentat de SRL "Rincor-
Prim", SRL "Electro", SRL "Elastic", SRL "BNV".
În rezultatul susţinerii financiare şi juridice corespunzătoare şi pe măsura
perfecţionării formelor de gestionare a pieţei o parte din aceste instituţii în
perspectivă pot să-şi extindă activitatea, astfel contribuind semnificativ la renovarea
economiei oraşului.

< Înapoi la [Cuprins](../../cuprins.md) | [3. Potențialul economic](../v1_cap3.md)