< Înapoi la [Cuprins](../../cuprins.md) | [3. Potențialul economic](../v1_cap3.md)

# 3.2. Prognoza potenţialului economic #

Reieşind din totalitatea condiţiilor şi factorilor or. Ungheni dispune de premise
favorabile pentru dezvoltarea socio-economică continue şi formarea economiei
oraşului ca centru regional polifuncţional.
Programul de dezvoltare socio-economică a or. Ungheni este elaborat în temeiul
evaluării şi analizei situaţiei existente a tuturor componentelor vitale ale oraşului:
procesele demografice, structura activităţii de producere, transport şi comunicaţii,
construcţii, comerţ şi servicii, gospodăria comunal - locativă, legăturile de producere,
sfera socială, resursele naturale, monumente de istorie, cultură şi arhitectură precum
şi starea mediului.22
Dezvoltarea potenţialului economic a oraşului se preconizează reieşind din
următoarelor direcţii de dezvoltare.
Industria
Reieşind din potenţialul de producere deja format al oraşului, cerinţele pieţei
interne şi posibilitatea promovării producţiei pe piaţa externă, principalele surse
trebuie concentrate spre dezvoltarea următoarelor ramuri ale industriei:
- ramura ce activează pe bază de materie primă locală cu păstrarea pieţei de
desfacere (industria alimentară, industria uşoară, industria materialelor de
construcţii);
- ramurile ce activează pe bază de materie primă importată şi dispun de piaţă de
desfacere, însă necesită restructurare (ramura industriei mobilei şi de
prelucrare a lemnului);
- industria ce contribuie la funcţionarea infrastructurii urbane, producerea şi
transportul energiei electrice, termice şi gaze naturale.
Reieşind din specializarea deja formată a producerii industriale nucleul
economiei oraşului rămâne a fi complexul agroindustrial şi de producere.
În structura producerii industriale îşi va păstra potenţialul de producere şi vor
căpăta impuls pentru dezvoltare următoarele ramuri:
industria alimentară şi prelucrătoare („Ungheni-Vin”, S.A. „Ecovit”, S.A.
„Cereale-Prut”, S.R.L. „Danova-Prim”, etc.);
- industria uşoară (ÎCS "Lear Corpration" S.R.L., S.A. „Covoare-Ungheni” etc,);
- industria materialelor de construcţii („Javelin N.A.” S.R.L., SRL "Cons-
Viand" S.R.L., „Ceramica-Ungheni” S.A. şi alte întreprinderi mici);
- alte întreprinderi ale business-ului mic şi mediu.
Sarcina primordială în dezvoltarea industriei oraşului constă în restructurarea şi
reprofilarea întreprinderilor de producere, modernizarea tehnologică a lor din contul
realizării politicii financiare şi fiscale, susţinerea activităţii de antreprenoriat în baza
dezvoltării business-ului mic şi mediu, crearea condiţiilor optime pentru atragerea
investiţiilor, participarea în realizarea proiectelor existente şi programelor de
cooperare internaţională.
Construcţii
Unul din factorii decisivi în restabilirea complexului de construcţii constă în
dezvoltarea pieţei investiţionale capabilă să satisfacă necesităţile economiei oraşului
cu capital fix.
Pentru implementarea acestui proces este necesară susţinerea investiţiilor locale
şi străine, încurajarea şi protecţia antreprenoriatului în domeniul construcţiilor şi
industriei materialelor de construcţii, sporirea calităţii lucrărilor de construcţii şi
materialelor de construcţii, inclusiv cele cu proprietăţi sporite de termoizolare, prin
implementarea tehnologiilor moderne, care vor reduce semnificativ consumul
resurselor energetice, termenul de execuţie a construcţiei obiectivelor noi, precum şi
costul total al fiecărui obiectiv, soluţionarea problemelor ce ţin de construcţiile
nefinisate, îmbunătăţirea situaţiei în domeniul construcţiilor locative prin intermediul
implementării diverselor mecanisme financiare şi de creditare, inclusiv susţinerea23
construcţiei ipotecare, reconstrucţia şi modernizarea caselor cu 2-5 nivele cu
utilizarea materialelor de construcţii noi fabricate în baza materiei prime locale cu
grad sporit de termoizolare.
Pentru realizarea programelor de dezvoltare socio-economică este necesar de
prevăzut mijloace pentru consolidarea bazei materiale a organizaţiilor de construcţii
existente iar în baza lor crearea întreprinderilor noi cu aceiaşi direcţie de dezvoltare
care vor presta servicii în construcţii atât pentru necesităţile oraşului cât şi în regiunea
de vest a republicii.
Comerţul interior
pentru organizarea pieţei angro şi elaborarea mecanismelor pentru utilizarea Sarcina
principală în dezvoltarea comerţului interior pentru perioada de calcul constă:
- perfecţionarea mecanismelor economice şi organizatorice, care vor stimula
legalizarea şi controlul sectorului neorganizat al pieţei interne;
- crearea condiţiilor eficientă a zonelor de producere existente;
- implicarea administraţiei şi conducerii oraşului în perfecţionarea şi
optimizarea reţelei de comerţ, în scopul evitării disproporţiilor în amplasarea
teritorială a întreprinderilor de comerţ;
- orientarea mecanismelor sanitare, antiincediare şi de supraveghere în scopul
stimulării dezvoltării infrastructurii de comerţ, întreprinderilor de comerţ de tip
contemporan şi cultura deservirii populaţiei.
Pentru îmbunătăţirea deservirii populaţiei în special asigurarea cu obiective de
comerţ, alimentare publică şi alte servicii în oraş este oportun crearea sistemului
organizat de centre publice cu diferenţierea exactă, amplasare optimă a obiectivelor
în limitele razei de deservire în îmbinare cu infrastructura transportului. Aceasta va
permite eliberarea nucleului centrului oraşului, amplasarea obiectivelor de prestări
servicii în apropierea zonelor rezidenţiale şi de muncă a populaţiei, sistematizarea
construcţiilor urbane, precum şi crearea aspectului arhitectural - estetic al centrelor
publice.

< Înapoi la [Cuprins](../../cuprins.md) | [3. Potențialul economic](../v1_cap3.md)