< Înapoi la [Cuprins](../../cuprins.md) | [1. Întroducere](../v1_cap1.md)

# 1.1. Obiectul lucrării #
Prezența lucrare este elaborată la comanda Întreprinderea Mixte "Protelco Geocad" SRL prin Caiet de sarcini pentru actualizarea documentației de urbanism, Contractul din 18 februarierie 2014 nr. 15691.

Tema centrul de sănătate publică raionul Ungheni din 19.03.2014 nr.020-189
Totodată ca bază legitimă pentru elaborare este valabilă Hotărîrea Guvernului Republicii Moldova din [07.12.2001 nr. 1362](https://www.legis.md/cautare/getResults?doc_id=44451&lang=ro#) "Cu privire la asigurarea localităților Republicii Moldova cu documentație de Urbanism și amenajarea teritoriului". (Abrogată de [HOTĂRÎREA Nr. 796 din 25-10-2012](https://www.legis.md/cautare/getResults?doc_id=11041&lang=ro))

Ca necesitate de elaborare sunt:
* Neconformarea documentației urbanistice aprobate anterior (obiect nr.14315/35,
nr.14430, nr-283B "Plan Urbanistic General or.Ungheni" elaborat în anul 1995 pe un
termen de calcul de 10 ani) condițiilor contemporane social-economice și politice, ce s-au creat și apar actualmente în Republică;

* Soluționarea problemelor de dezvoltare a teritoriului în legătură cu schimbările
structurii în domeniul construcțiilor locative și a infrastructurii sociale, aplicarea
soluțiilor moderne privind dezvoltarea sistemului de dotări tehnice, perfecționarea
rețelei de drumuri și străzi, dezvoltarea transportului public;

* Optimizarea ulterioară a zonării teritoriului ținînd cont de factorii ecologici, sanitari pentru dezvoltarea mediului urban.

Așadar acest memoriu în volum de elaborare a proiectului sus numit reflectă următoarele categorii de probleme și într-o măsură mai mică sau mai mare determină soluțiile de realizare a propunerilor de proiect:

* Probleme principale rezultate din analiza situației existente, disfuncționalități și
priorități;

* Volumul și structura potențialului uman, resurse ale muncii;

* Potențialul economic al orașului;

* Situația fondului locativ;

* Organizarea circulației și transporturilor;

* Echipa tehnico-edilitară;

* Reabilitarea, protecția și conservarea mediului;

Lucrarea, conform Temei - program urmărește scopul de a examina situația existentă a teritoriului orașului și de a compara cu deciziile de proiect acceptate și adaptate în lucrarea anterioară în ce privește nivelul de amenajare a teritoriului cît și complexitatea obiectivelor de utilitate publică necesare pentru unul din principalele orașe ale Republicii Moldova cît și compararea cu condițiile contemporane de existență, măsura de realizare a PUG-ului precedent.

În urma studiului și a propunerilor de proiect destinate soluționării problemelor menționate mai sus proiectul oferă metode și instrumente de acțiuni necesare atît elaborării, aprobării cît și urmării aplicării planului urbanistic general în următoarele domenii: proiectare, administrare centrală și locală, agenți economici, colectivități sau persoane beneficiare.

S-a examinat starea spațiului locativ a zonelor locuibile ocupate cu locuințe în regim mic de înălțime și blocuri locative multietajate cu apartamente colective cît și posibilitatea de păstrare, reanimare și destinație a acestor spații locative, nivelul lor de amenajare urbanistică complexitatea de utilitate publică, asigurarea acestora cu spații verzi, parcuri, scuaruri, terenuri sportive etc. precum și cu structura edilitară.

După analiza și sistematizarea circulației transportului urmează să fie elaborate reglementări de organizare a circulației transportului de sens major, cît și a terenurilor pentru parcări, amplasarea terenurilor pentru construcția garajelor cooperative.

Este necesar de preconizat măsuri de organizare a timpului liber al populației cum: sportul, turismul, odihna la un nivel cultural înalt, pentru care sunt necesare acțiuni de amplasare a întreprinderilor și instituțiilor de odihnă, agrement și sport, de amenajare a parcurilor, scuarurilor cît și trasarea unor noi străzi, drumuri și construcția noilor obiective.

La etapa culegerii datelor inițiale pentru proiectare s-a examinat favorabilitatea terenurilor pentru construcții în hotarele orașului și a terenurilor imediat adiacente, s-au propus măsuri și acțiuni contra proceselor geologice periculoase cît și pentru păstrarea spațiului ambiant.

A apărut necesitatea de a scoate la iveală și a amenaja monumentele istorice și arhitecturale cît și a organiza spre acestora rute turistice.

Nu este la nivel situația în economie și industrie. Au fost extrase din activitatea economică un rînd de întreprinderi și instituții de sens unional. S-a redus baza de marfă primă și de piață de desfacere, multe întreprinderi s-au dezvoltat și-au întrerupt activitatea, și devin învechite atît fizic cît și moral. Una din problemele - cheie în condițiile actuale este problema ecologică, pentru rezolvarea căreia este necesar de o cantitate majoră de mijloace materiale și fizice.

Orașul nu dispune de o divizare strictă între zonele de locuit și cele de producere.

Multe cartiere locative ale orașului nu dispun, sau dispun insuficient de rețele edilitare. 

Problema dominantă cotidiană a localității în cauză este organizarea circulației transportului atît public cît și celui de marfă.

În documentația de urbanism pentru municipiul Ungheni sunt stimulate metode
contemporane de sistematizare, care evidențiază problema reformării consecutive a teritoriului și crearea condițiilor optime vitale a populației.

< Înapoi la [Cuprins](../../cuprins.md) | [1. Întroducere](../v1_cap1.md)