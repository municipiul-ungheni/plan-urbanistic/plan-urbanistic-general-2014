< Înapoi la [Cuprins](../../cuprins.md) | [1. Întroducere](../v1_cap1.md)

# 1.2 Colectivul de elaborare

La elaborarea Planului Urbanistic General al or.Ungheni au participat ca autori ai proiectului specialişti a INCP "Urbanproiect":

| Funcția                              | nume             |
|--------------------------------------|------------------|
| Director general INCP “Urbanproiect” | Iurie Povar      |
| Arhitect şef INCP “Urbanproiect”     | Vladimir Bocacev |
| Director DU şi AT                    | R. Alexeev       |
| Arhitector şef or.Ungheni            | Vlad Savin       |
| Inginer şef proiect                  | N.Luchianova     |
| Arhitect şef proiect                 | L.Mămăligă       |

Concomitent la elaborare au participat:

| Funcția                                                                                 | nume            |
|-----------------------------------------------------------------------------------------|-----------------|
| Specialist principal. Şef dir.economie.                                                 | S. Vorobiova    |
| Specialist principal. Şef dir.industrie.                                                | A. Chicu        |
| Specialist principal. Hidrogeotehnic                                                    | V. Davidenco    |
| Specialist principal. Căi de comunicaţii şi transport                                   | T. Mironova     |
| Specialist principal. Alimentarea cu apă potabilă                                       | V. Covalenco    |
| Specialist principal. Alimentarea cu gaze                                               | A. Lîcova       |
| Specialist principal. Alimentarea cu energie electrică                                  | E. Galikovskaia |
| Specialist principal. Canalizare şi salubrizarea teritoriului salubrizarea teritoriului | S. Pînzaru      |
| Specialist principal. Şef CPCE. Protecţia mediului.                                     | F. Munteanu     |
| Specialist principal. Protecţia civilă.                                                 | V. Mutaf        |


< Înapoi la [Cuprins](../../cuprins.md) | [1. Întroducere](../v1_cap1.md)