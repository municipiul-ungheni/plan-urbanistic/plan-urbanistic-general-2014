< Înapoi la [Cuprins](../../cuprins.md) | [1. Întroducere](../v1_cap1.md)

# 1.3 Surse documentare #

Ca bază pentru elaborarea " PUG or. Ungheni" s-au utilizat următoarele lucrări şi proiecte elaborate anterior:

* obiect nr.14315/35, nr.14430, nr-283B "Plan Urbanistic General or.Undgeni" elaborat în anul 1995, INCP "Urbanproiect" Chişinău, ICPUAT "Urbanproiect' Bucureşti.
* obiect nr.12652 "Проект генерального плана г.Унгены" elaborat în anul 1987 ГПИ "Молдгипрострой"
* obiect nr.11776 "ПДП северного жилого района г.Унгены" elaborat în anul 1980 ГПИ "Молдгипрострой"
* obiect nr.13668 "Проект детальной планировки жилого района Дануцены г.Унгены" elaborat în anul 1989 ГПИ "Молдгипрострой"
* obiect nr.13692 "Проект детальной планировки центра г.Унгены elaborat în anul 1993 ГПИ "Urbanproiect"
* obiect nr.4080/12652 "Отчет по инженерным изысканиям, выполненный для разработки генерального плана г.Унгень elaborat în anul 1986 ГИИТИ "МолдГИИнтиз".

< Înapoi la [Cuprins](../../cuprins.md) | [1. Întroducere](../v1_cap1.md)