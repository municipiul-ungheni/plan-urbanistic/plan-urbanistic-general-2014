< Înapoi la [Cuprins](../../cuprins.md) | [1. Întroducere](../v1_cap1.md)

# 1.5 Relații în teritoriu #
Orașul Ungheni s-a dezvoltat pe partea stângă a râului Prut la confluența cu pârâul Delia, situat la o atitudine cuprinsă între 32-120m, așezarea este mărginită de poalele dealului Bălănești, dealuri cu altitudinea 426m în partea sud - estică a raionului Ungheni. Așezarea este considerată ca aparținând luncii Prutului.

Partea nord-estică a raionului și partea sud-estică este semnalată în regiuni împădurite care nu ating mai mult de 400m altitudine. Predomină însă terenul arabil, pășunile și livezile.

Teritoriul raionului este străbătut în afara râului Prut de alte cinci râuri:
* Vladnic, 
* Șoltoaia, 
* Delia, 
* Cula, 
* Gala Mare. 

Nivelul apei freatice în zona orașului Ungheni măsoară de la 36 la 52 metri.

Solul este cernoziom obișnuit, se găsesc soluri din argilă nisipoasă, brune-cenușii și aluviale.

Zona orașului Ungheni este considerată a fi gradul 7 seismic.

Clima acestor meleaguri aparține sectorului cu climă temperat-continentală caracterizându-se prin ierni relativ călduroase cu puțină zăpadă și veri lungi și foarte călduroase. O caracteristică a climei este variația de temperatură și precipitații.

Temperatura medie anuală este 9&deg;C-10&deg;C. Media lunii celei mai reci Ianuarie variază între -3&deg;C și -4&deg;C. Temperatura lunii cele mai calde, iunie, este de 21&deg;C-22&deg;C. Repartiția lunară a cantității de precipitații este de 55mm. Viteza medie a vântului cel mai frecvent este de 4,1m/s iar frecvența medie a aceluiași vânt este de 21,5%.

Lângă trupul mare a orașului Ungheni cu o suprafață de 1643ha sunt situate: 
* trupul 1: Rezerva de apă 1,5ha
* trupul 3 - Groapa de gunoi 3,5ha 
* Stația de epurare situată lângă satul Valea Mare, în sudul orașului Ungheni cu o suprafață de 4,75ha.

Pe direcția est-vest și nord-est orașul Ungheni este străbătut de o cale ferată simplă cu echipament lărgit neeletrificată. Trei gări de pasageri situate: două în cartierul Central și una în est de cartierul Berești precum și o gară de mărfuri situată în estul localității, la Untești, asigură deservirea pe calea ferată.

Deservirea pe calea ferată asigurând legăturile cu localitățile din raion este asigurată de o autogară situată tot în cartierul Central.

Tabel: [Indicii principali pe municipiul Ungheni](https://docs.google.com/spreadsheets/d/1VrVJ4QFBXyooKCdAbi85lD0b8U7jcGa6dXO-ZEt3jgw/edit?usp=sharing)


< Înapoi la [Cuprins](../../cuprins.md) | [1. Întroducere](../v1_cap1.md)