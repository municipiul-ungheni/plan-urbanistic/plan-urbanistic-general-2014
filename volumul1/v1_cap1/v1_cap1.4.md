< Înapoi la [Cuprins](../../cuprins.md) | [1. Întroducere](../v1_cap1.md)

# 1.4. Scurt istoric #
Orașul Ungheni este situat pe malul stâng al râului Prut, în partea vestică a Republicii Moldova.

Este atestat pentru prima dată în cronicile domnești în anul 1462. se presupune că denumirea "Ungheni" se datorează amplasării sale în meandrele râului Prut a cărui albie formează aici unghiuri proeminente.

Din cele mai vechi timpuri au existat două așezări gemene pe ambele maluri ale râului Prut, cu case construite din lemn, din nuiele și argilă, mai târziu din acoperiș de paie sau stuf. Îndeletnicirile locuitorilor erau cele tradițional țărănești: agricultura, creșterea animalelor, pescuitul. Situarea la o distanță foarte apropiată
(aproximativ 20km) de orașul Iași, capitala Principatului Moldova a condiționat dezvoltarea comerțului și a meșteșugarilor. După 1812 râul Prut devine hotarul ce desparte localitățile gemene cauzând evoluția lor separată.

Așezarea de pe malul stâng - actualul orașul Ungheni - situată pe terasa superioară a luncii Prutului în anul 1817 numără 44 case. Construcția căii ferate Chișinău - Iași, exploatarea căreia începe în 1875, iar mai târziu și construcția în anii 1914 - 1917 a căii ferate Ungheni - Bălți impulsionează dezvoltarea localității ca un important nod de circulație. În această perioadă apar aici ateliere de reparații și de deservire a căilor ferate, se dezvoltă comerțul. Crește respectiv și numărul populației din sat, care la începutul secolului XX avea de acum 580 case.

Datorită construirii localității Ungheni ca un important nod de cale ferată, în 1940 i se atribuie statutul de oraș. După cel de-al doilea război mondial începe dezvoltarea industriei prin construirea unei importante uzine biochimice.

În anul 1964 orașul devine centrul raionului administrativ - teritorial Ungheni, iar în 1971 centru de subordonare republicană.

În această perioadă în corpul orașului este inclus satul Dănuțeni, iar mai târziu și satul Berești, se amenajează străzile, apar primele clădiri multietajate.

Mult timp orașul făcea parte din zona de hotar, ceea ce și-a impus aparența prin anumite restricții de liberă circulație a cetățenilor, prin interzicerea accesului la râul Prut, rolul căruia ca axă de dezvoltare arhitectural - urbanistică este neglijat. Pentru odihna locuitorilor a fost amenajat un lac artificial prin îndiguirea râului Delia.

În anii 1970 se construiește centrul social - administrativ al orașului cu piața publică centrală, se formează zona de întreprinderi industriale, în special cu profil de prelucrare a producției agricole, de construcții și transport. Este dat în exploatare cel mai mare combinat de covoare din Republică.

Dezvoltarea fondului locuibil a fost direcționată spre construcția de locuințe colective cu tronsoane tip panouri mari, având înălțimea de P + 8 etaje. De la începutul anilor 1980 se observă o schimbare de fond, accentul punându-se pe construcția de case particulare cu lot, condițiile economice impunând sistarea aproape în totalitate a edificării de blocuri colective.

Actualmente orașul Ungheni numără 38 200 locuitori și se întinde pe o suprafață de 1643 ha.

< Înapoi la [Cuprins](../../cuprins.md) | [1. Întroducere](../v1_cap1.md)