< Înapoi la [Cuprins](../cuprins.md)
# 11. Unele permisiuni și restricții. Reglementări #


În scopul uşurării mediului de aplicare a prevederilor şi diverselor măsuri al
Planului Urbanistic General al oraşului Ungheni s-au stabilit, din punct de vedere
urbanistic, cu ajutorul unui sistem de reglementări, regimul juridic din cadrul
localităţii. Astfel s-au propus următoarele reglementări şi categorii de intervenţii:
- După etapa de proiectare ''PUG'' trebuie să urmeze etapele ulterioare de elaborare
''PUZ'' ;
- Terenurile destinate zonei de locuit existente sunt situate în intravilanul existent
al urbei, cu excepţia terenurilor libere de construcţii, destinate pentru construcţiile
ulterioare perspective, situate în extravilan şi incluse în intravilan pentru termenul de
calcul;
- Terenurile cu locuinţe existente nu se predau reîmpărţirii, cu excepţia terenurilor
adiacente terenului destinat pentru trasarea unor segmente de străzi şi drumuri
propuse;
- Obiectivele de utilitate publică se construiesc, în temei, pe terenuri libere sau
după demolarea construcţiilor cu uzura definitivă;
- Pentru toate tipurile de locuinţe se va urmări asigurarea unui grad ridicat de
confort şi înalt grad, după posibilităţi, de utilizare tehnico-sanitară. Terenurile
destinate zonei de circulaţie vor suporta lărgirea carosabilului pînă la limita necesară
în zona cu locuinţe existente, cît şi trasarea noilor străzi, conform normelor şi
cerinţelor de circulaţie, realizarea diverselor intersecţii la nivel în scopul degravării
circulaţiei şi al necesităţilor de trafic.
Terenurile destinate zonelor de spaţii verzi vor suporta următoarele intervenţii:
- realizarea zonelor de agrement, turistice şi sportive;
- realizarea unor parcuri, grădini şi scuaruri în lunca inundabilă a r. Prut, cît şi în
zonele de amplasare a noilor cartiere locative;
- realizarea noilor complexe sportive în luncile inundabile menţionate (stadionul
raional, centrul de canotaj, etc.);
- reamenajarea şi punerea la valoare a unor zone de agrement;
 construirea unor bariere de protecţie sanitară împotriva poluării;107
- solvare rezervelor pentru amplasamente de obiective publice;
- creare perdele verzi de protecţie la hotarele de producere.
Terenurile destinate zonei industriale şi de producere agricolă vor suporta:
- rezervări de amplasamente pentru extinderi justificate;
- rezervări pentru încurajarea iniţiativei particulare pentru micul bussines;
- rezervarea pentru extinderi şi amplasamente edilitare;
- rezerva pentru trasee ale conductelor edilitare;
- protejarea zonelor sanitare a sondelor de apă arteziană cu raza nu mai mică de 30
metri;
- amenajarea terenului pentru utilizarea deşeurilor solide;
- rezerva pentru extinderea cimitirului.
O altă categorie de intervenţii sunt interdicţiile temporare (provizorii) sau
definitive de construire:
a) Interdicţie temporară de construire se instituie deobicei pentru zone care necesită
studii şi cercetări suplimentare, ca zona centrală, zonele cu cartierele şi străzile noi,
cît şi artere de circulaţie, zonele cu amenajări de intersecţii.
Pe toate acestea se vor elabora planuri urbanistice fie zonale, fie de detaliu.
Restricţiile sunt valabile pe o perioadă de 12 luni, pînă la 2 ani de la aprobarea
prezentului PUG.
b) Interdicţiile definitive de construire se instituie:
- în lunca posibil inundabilă a r. Prut fără măsuri de protecţie sporit a apelor
freatice şi pericolului de inundare de apele de suprafaţă şi revărsate a rîului sus numit;
- la o distanţă mai mică de 4m de aliniament (pentru locuinţe).
Restricţiile privitoare la regimul de înălţime:
- pentru locuinţele individuale, în parcele se instituie un regim de înălţime P
sau P + 1;
- pentru blocurile locative cu apartamente colective numărul de nivele corespunde
numărului de nivele a blocurilor învecinate.

< Înapoi la [Cuprins](../cuprins.md)