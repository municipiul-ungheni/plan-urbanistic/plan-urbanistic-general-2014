< Înapoi la [Cuprins](../cuprins.md)

# 3. Potențialul economic #
* [3.1. Caracterizarea situației existente](v1_cap3/v1_cap3.1.md)
* [3.2. Prognoza potențialului economic](v1_cap3/v1_cap3.2.md)

< Înapoi la [Cuprins](../cuprins.md)